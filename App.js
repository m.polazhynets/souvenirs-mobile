/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import i18n from './src/utils/i18n';
import { PersistGate } from 'redux-persist/integration/react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './src/navigator/RootNavigation';
import configureStore from './src/store/configureStore';
import RootStackScreen from './src/navigator/Root';

console.reportErrorsAsExceptions = false;
console.disableYellowBox = true;

const { store, persistor } = configureStore();
;

const getActiveRouteName = state => {
  const route = state.routes[state.index];

  if (route.state) {
    // Dive into nested navigators
    return getActiveRouteName(route.state);
  }

  return route.name;
};


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRouteName: null
    }
    this.routeNameRef = React.createRef();
  }

  render() {
    const { is_authorized } = store.getState().authReducer;
    const { currentRouteName } = this.state;

    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <I18nextProvider i18n={i18n}>
            <SafeAreaProvider>
              <NavigationContainer
                ref={navigationRef}
                onStateChange={state => {
                  const previousRouteName = this.routeNameRef.current;
                  const currentRouteName = getActiveRouteName(state);

                  if (previousRouteName !== currentRouteName) {
                    this.setState({ currentRouteName })
                  }
                  this.routeNameRef.current = currentRouteName;
                }}
              >
                <RootStackScreen currentRouteName={currentRouteName} is_authorized={is_authorized}/>
              </NavigationContainer>
            </SafeAreaProvider>
          </I18nextProvider>
        </PersistGate>
      </Provider>
    );
  }
};


export default App