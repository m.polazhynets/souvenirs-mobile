import { combineReducers } from 'redux';
import authReducer from './AuthReducer';
import settingsReducer from './SettingsReducer';

const rootReducer = combineReducers({
   authReducer,
   settingsReducer
});

export default rootReducer;