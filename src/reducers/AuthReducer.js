import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import { AUTH } from '../constants/ActionTypes';

export default injectReducer(initialState.authReducer, {
  // [AUTH.GET_REGISTRATION_DATA_REQUEST]: (state) => ({
  //   ...state,
  //   is_general_request: true
  // }),
  // [AUTH.GET_REGISTRATION_DATA_SUCCESS]: (state, { payload }) => ({
  //   ...state,
  //   registration_data: payload,
  //   is_general_request: false
  // }),
  // [AUTH.GET_REGISTRATION_DATA_FAILURE]: (state) => ({
  //   ...state,
  //   is_general_request: false
  // }),
  [AUTH.SET_IS_AUTHORIZED]: (state, { payload: { is_authorized } }) => ({
    ...state,
    is_authorized,
    access_token: true 
  }),

  [AUTH.LOG_OUT]: () => ({
    ...initialState.authReducer
  })
})