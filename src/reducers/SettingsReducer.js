import initialState from '../store/InitialStates';
import injectReducer from '../store/InjectReducer';

import { SETTINGS } from '../constants/ActionTypes';

export default injectReducer(initialState.settingsReducer, {
  [SETTINGS.NETWORK_CONNECTION_CHANGE]: (state, { payload }) => ({
    ...state,
    network_connected: payload
  }),
})