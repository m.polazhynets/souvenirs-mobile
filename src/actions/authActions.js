import { createActions } from 'redux-actions';

import { AUTH } from '../constants/ActionTypes';

export const { 
  setIsAuthorized, 
} = createActions({
  [AUTH.SET_IS_AUTHORIZED]: (is_authorized) => ({is_authorized})
});