import { Dimensions, Platform } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper';
import { COLOR_BLACK } from './Colors';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window')
const localWidth = screenWidth >= screenHeight ? screenHeight : screenWidth
const localHeight = screenWidth < screenHeight ? screenHeight : screenWidth
const widthCoef = ((localWidth > 375) ? 375 : localWidth)/ 375
const heightCoef = ((localHeight > 812) ? 812 : localHeight)/ 812

export const scale = (size) => widthCoef * size
export const v_scale = (size) => heightCoef * size
export const deviceWidth = screenWidth
export const deviceHeight = screenHeight

const variables = {
  fontSize: {
    smaller: scale(10),
    small: scale(11),

    regularSmall: scale(13), 
    regular: scale(15), 
    mainRegular: scale(17), 
    mediumRegular: scale(18), 
    large: scale(20),
    xLarge: scale(22),
    biggerlarge: scale(24), 

    extLarge: scale(28), 
    extraLarge: scale(32), 
    megaLarge: scale(34),
  },
  shadowSmall: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: Platform.OS === 'ios' ? 15 : 7
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: scale(34),
    letterSpacing: 0.41,
    color: COLOR_BLACK
  }
}

export const headerHeight = (isIphoneX) ? v_scale(80) : v_scale(60)

export const FONT_BOLD_DISPLAY = 'SFProDisplay-Bold'
export const FONT_SEMIBOLD_DISPLAY = 'SFProDisplay-Semibold'
export const FONT_REGULAR_DISPLAY = 'SFProDisplay-Regular'
export const FONT_BOLD = 'SFProText-Bold'
export const FONT_REGULAR = 'SFProText-Regular'
export const FONT_MEDIUM = 'SFProText-Medium'
export const FONT_LIGHT = 'SFProText-Light'

export default variables
