export const COLOR_WHITE = '#FFFFFF';
export const COLOR_GRAY_1 = '#8A8A8F';
export const COLOR_GRAY_2 = '#A3A3A3';
export const COLOR_GRAY_3 = 'rgba(142, 142, 147, 0.12)';
export const COLOR_GRAY_4 = '#C7C7CC';
export const COLOR_GRAY_5 = '#EFEFF4';
export const COLOR_GRAY_6 = '#828282';
export const COLOR_ORANGE = '#FF9500';
export const COLOR_RED = '#EB5757';
export const COLOR_BLUE = '#007AFF';
export const COLOR_BLACK = '#000000';
export const COLOR_BLACK_1 = '#333333';
export const COLOR_GREEN = '#4CD964';

