/** IMAGES */
export const BACKGROUND_IMAGE = require('../../assets/images/background.png');
export const LEVEL_LION_IMAGE = require('../../assets/images/level_lion.png');
export const QR_CODE_IMAGE = require('../../assets/images/qr_code.png');
export const USER_DEFAULT_IMAGE = require('../../assets/images/user_default.png');
export const CORNER_IMAGE = require('../../assets/images/corner.png');
export const DEMO_1_IMAGE = require('../../assets/images/demo_1.png');

/** ICONS */
export const FULL_LOGO_ICON = require('../../assets/images/icons/full_logo.png');
export const LOGO_ICON = require('../../assets/images/icons/logo.png');
export const ARROW_ICON = require('../../assets/images/icons/arrow.png');
export const ARROW_WHITE_ICON = require('../../assets/images/icons/arrow_white.png');
export const ARROW_RIGHT_GRAY_ICON = require('../../assets/images/icons/arrow_right_gray.png');
export const ARROW_TOP_BLUE_ICON = require('../../assets/images/icons/arrow_top_blue.png');
export const ARROW_BOTTOM_GRAY_ICON = require('../../assets/images/icons/arrow_bottom_gray.png');
export const PLUS_ICON = require('../../assets/images/icons/plus.png');
export const CLOSE_SMALL_BLACK_ICON = require('../../assets/images/icons/close_small_black.png');
export const FACE_SMALL_BLACK_ICON = require('../../assets/images/icons/face_small_black.png');
export const FACE_BLUE_ICON = require('../../assets/images/icons/face_blue.png');
export const MULTIPLE_USERS_ICON = require('../../assets/images/icons/multiple_users.png');
export const COPY_ICON = require('../../assets/images/icons/copy.png');
export const SEARCH_ICON = require('../../assets/images/icons/search.png');
export const DICTATION_ICON = require('../../assets/images/icons/dictation.png');
export const CALL_ICON = require('../../assets/images/icons/call.png');
export const MAP_ICON = require('../../assets/images/icons/map.png');

export const HOME_TAB_ICON = require('../../assets/images/icons/tabs/home.png');
export const HOME_TAB_ACTIVE_ICON = require('../../assets/images/icons/tabs/home_active.png');
export const SALE_TAB_ICON = require('../../assets/images/icons/tabs/sale.png');
export const SALE_TAB_ACTIVE_ICON = require('../../assets/images/icons/tabs/sale_active.png');
export const CODE_TAB_ICON = require('../../assets/images/icons/tabs/code.png');
export const CODE_TAB_ACTIVE_ICON = require('../../assets/images/icons/tabs/code_active.png');
export const PROFILE_TAB_ICON = require('../../assets/images/icons/tabs/profile.png');
export const PROFILE_TAB_ACTIVE_ICON = require('../../assets/images/icons/tabs/profile_active.png');
export const MENU_TAB_ICON = require('../../assets/images/icons/tabs/menu.png');
export const MENU_TAB_ACTIVE_ICON = require('../../assets/images/icons/tabs/menu_active.png');

export const FILE_TEXT_INFO_ICON = require('../../assets/images/icons/menu/file_text_info.png');
export const FILE_TEXT_ICON = require('../../assets/images/icons/menu/file_text.png');
export const HELP_CIRCLE_OUTLINE_ICON = require('../../assets/images/icons/menu/help_circle_outline.png');
export const MESSAGE_ICON = require('../../assets/images/icons/menu/message.png');
export const SHARE_ICON = require('../../assets/images/icons/menu/share.png');