/**
 * @module Sagas/Prizes
 * @desc Auth
 */

import { all, put, takeLatest, call } from 'redux-saga/effects';
import { request } from '../utils/Client';
import { AUTH } from '../constants/ActionTypes'

// export function* getWinnersList() {
//   try {
//     const response = yield call(
//       request,
//       `staff/prizes/winners`,
//       {
//         method: 'GET',
//       }
//     );
//     yield put({
//       type: ActionTypes.GET_WINNERS_LIST_SUCCESS,
//       payload: response,
//     });
//   } catch (err) {
//     yield put({
//       type: ActionTypes.SET_ERROR_MESSAGE,
//       payload: {message: err.statusMeans},
//     });
//     /* istanbul ignore next */
//     yield put({
//       type: ActionTypes.GET_WINNERS_LIST_FAILURE,
//       payload: err,
//     });
//   }
// }


/**
 * Prizes Sagas
 */
export default function* root() {
  yield all([
    // takeLatest(ActionTypes.GET_WINNERS_LIST, getWinnersList),
  ]);
}
