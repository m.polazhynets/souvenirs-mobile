import { applyMiddleware, createStore, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import NetInfo from "@react-native-community/netinfo";
import storage from '@react-native-community/async-storage';
import { SETTINGS } from '../constants/ActionTypes';
import rootSaga from '../sagas';
import rootReducer from '../reducers';

import middleware, { sagaMiddleware } from './middleware';

const persistedReducer = persistReducer(
  {
    key: 'root',
    storage,
    whitelist: ['authReducer', 'settingsReducer'],
  },
  rootReducer,
);

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(persistedReducer, composeEnhancer(applyMiddleware(...middleware)));

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(require('../reducers/index').default);
    });
  }

  let persistor = persistStore(store, null, () => {
    NetInfo.fetch().then(state => {
      store.dispatch({
        type: SETTINGS.NETWORK_CONNECTION_CHANGE,
        payload: state.isConnected,
      });
    })
  });

  return { store, persistor }
}