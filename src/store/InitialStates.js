export default {
  authReducer: {
    is_request: false,
    is_authorized: false,
    access_token: false
  },
  settingsReducer: {
    network_connected: false,
  },
}