import { StyleSheet, Platform } from 'react-native';
import { COLOR_RED, COLOR_BLACK, COLOR_WHITE } from '../../../constants/Colors';
import variables, { FONT_REGULAR, scale, v_scale } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export const placeholderColor = 'rgba(0, 0, 0, 0.4)';

export default StyleSheet.create({
  inputWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },  
  container: {
    backgroundColor: COLOR_WHITE
  },
  input: {
    flex: 1,
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    paddingHorizontal: 0,
    paddingVertical: Platform.OS === 'android' ? v_scale(0) : v_scale(1),
    color: COLOR_BLACK,
    letterSpacing: -0.41,
    borderColor: 'white'
  },
  inputContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    borderWidth: scale(1),
    borderColor: 'rgba(0, 0, 0, 0.08)',
    backgroundColor: 'rgba(0, 0, 0, 0.08)', 
    paddingVertical: Platform.OS === 'android' ? v_scale(8) : v_scale(12),
    paddingHorizontal: scale(12),
    borderRadius: scale(8),
    marginBottom: v_scale(8)
  },
  errorStyle: {
    borderColor: COLOR_RED
  },
  placeholder: {
    color: 'red',
  },
  noMargin: {
    marginBottom: 0
  },
  labelWrapper: {
    width: scale(110),
    paddingVertical: Platform.OS === 'android' ? v_scale(5) : v_scale(4),
  },
  label: {
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    color: COLOR_BLACK,
    opacity: 0.4,
    letterSpacing: -0.41,
  }
});
