import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, Text } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import styles, { placeholderColor } from './styles';

class Input extends Component {

  handleBlur = () => {
    const { onBlur } = this.props;

    if (onBlur) onBlur();
  }

  render() {
    const { mask, value, containerStyles, inputStyles, placeholderTextColor, maskProps, required, noMargin, isValid, rightContent = null, label, ...props } = this.props;

    let _maskProps = {}, Component = TextInput;
    if (mask) {
      _maskProps = maskProps || {
        type: 'custom',
        options: {
          withDDD: true,
          mask
        }
      };
      Component = TextInputMask;
    }

    return (
      <View style={styles.container}>
        <View style={[styles.inputContainerStyle, noMargin && styles.noMargin, containerStyles, (required && isValid === false) && styles.errorStyle]} >
          <View style={styles.inputWrapper}>
            {label && (
              <View style={styles.labelWrapper}>
                <Text numberOfLines={1} style={styles.label}>{label}</Text>
              </View>
            )}
            <Component
              {...props}
              {..._maskProps}
              value={value}
              autoCapitalize={'none'}
              style={[styles.input, (required && isValid === false) && styles.inputError, inputStyles]}
              placeholderTextColor={(placeholderTextColor) ? placeholderTextColor : placeholderColor}
              onBlur={this.handleBlur}
            />
            {rightContent}
          </View>
        </View>
      </View>
    )
  }
}

export default Input;

Input.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  mask: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  required: PropTypes.bool,
  noMargin: PropTypes.bool,
  isValid: PropTypes.bool,
  containerStyles: PropTypes.object,
  onBlur: PropTypes.func,
  inputStyles: PropTypes.object,
};