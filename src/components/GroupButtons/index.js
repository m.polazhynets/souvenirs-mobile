import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from './styles';

export default GroupButton = ({
  data,
  onPress,
  active,
  stylesButton = {},
  stylesContainer = {},
}) => {

  return (
    <View style={[styles.container, stylesContainer]}>
      {
        data.map((item) => (
          <TouchableOpacity
            key={item.key}
            activeOpacity={0.8}
            onPress={() => onPress(item.key)}
            style={[styles.button, stylesButton, (active == item.key) && styles.activeButton]}
          >
            <Text style={[styles.text, (active == item.key) && styles.activeText]}>{item.title}</Text>
          </TouchableOpacity>
        ))
      }
    </View>
  );
}

GroupButton.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  })).isRequired,
  active: PropTypes.number.isRequired,
  onPress: PropTypes.func,
  stylesContainer: PropTypes.any,
  stylesButton: PropTypes.object,
};