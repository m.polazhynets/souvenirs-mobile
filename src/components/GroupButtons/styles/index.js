import { StyleSheet } from 'react-native';
import { COLOR_BLUE, COLOR_WHITE } from '../../../constants/Colors';
import variables, { scale, FONT_REGULAR } from '../../../constants/StylesConstants';

const { regular } = variables.fontSize;


export default StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: COLOR_BLUE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: scale(4)
  },
  button: {
    padding: scale(10),
    width: '50%'
  },
  activeButton: {
    backgroundColor: COLOR_BLUE,
  },
  text: {
    textAlign: 'center',
    color: COLOR_BLUE,
    fontSize: regular,
    fontFamily: FONT_REGULAR,
    letterSpacing: -0.24
  },
  activeText: {
    color: COLOR_WHITE,
  }
})