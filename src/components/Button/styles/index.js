import { StyleSheet } from 'react-native';
import { COLOR_WHITE, COLOR_BLUE } from '../../../constants/Colors';
import variables, { scale, v_scale, FONT_BOLD } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  buttonText: {
    textAlign: 'center',
    color: COLOR_WHITE,
    fontSize: mainRegular,
    fontFamily: FONT_BOLD,
    letterSpacing: -0.41
  },
  button: {
    position: 'relative',
    paddingVertical: v_scale(14),
    backgroundColor: COLOR_WHITE,
    borderWidth: scale(2),
    borderColor: COLOR_WHITE,
    width: '100%',
    borderRadius: scale(10)
  },
  buttomMargin: {
    marginBottom: v_scale(18)
  },
  button_primary: {
    backgroundColor: COLOR_BLUE,
    borderColor: COLOR_BLUE
  },
  button_white: {
    backgroundColor: COLOR_WHITE,
    borderColor: COLOR_BLUE
  },
  buttonText_primary: {
    color: COLOR_WHITE
  },
  buttonText_white: {
    color: COLOR_BLUE
  },
  loaderWrap: {
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    top: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  }
})
