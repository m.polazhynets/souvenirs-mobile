import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, ActivityIndicator, View } from 'react-native';
import { COLOR_ROYAL_BLUE, COLOR_WHITE } from '../../constants/Colors';
import styles from './styles';

export default Button = ({
  onPress,
  isLoading,
  contentContainerStyle = {},
  style = {},
  disable,
  title,
  type,
  margin,
}) => {
  const activeOpacity = disable ? 1 : 0.8;

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      onPress={() => {
        if (disable || isLoading) return;
        onPress();
      }}
      style={[styles.button, margin && styles.buttomMargin, type && styles[`button_${type}`], contentContainerStyle]}
    >
      {
        isLoading ? <View style={styles.loaderWrap}><ActivityIndicator size="small" color={(type === 'primary') ? COLOR_WHITE : COLOR_ROYAL_BLUE} /></View> : <View />
      }
      <View style={isLoading && { opacity: 0 }}>
        <Text style={[styles.buttonText, type && styles[`buttonText_${type}`], style]}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf([
    'primary',
    'white',
  ]),
  disable: PropTypes.bool,
  isLoading: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  margin: PropTypes.bool,
  contentContainerStyle: PropTypes.any,
  style: PropTypes.object,
};