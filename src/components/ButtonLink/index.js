import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native';
import styles from './styles';

export default ButtonLink = ({
  onPress,
  contentContainerStyle = {},
  style = {},
  disable,
  title,
}) => {

  const activeOpacity = disable ? 1 : 0.8;

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      onPress={() => {
        if (disable) return;
        onPress();
      }}
      style={[styles.link, contentContainerStyle]}
    >
      <Text style={[styles.linkText, style]}>{title}</Text>
    </TouchableOpacity>
  );
}

ButtonLink.propTypes = {
  title: PropTypes.string.isRequired,
  disable: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  contentContainerStyle: PropTypes.any,
  style: PropTypes.object,
};