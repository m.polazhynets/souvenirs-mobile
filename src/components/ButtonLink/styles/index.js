import { StyleSheet } from 'react-native';
import { COLOR_BLUE } from '../../../constants/Colors';
import variables, { scale, FONT_MEDIUM } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  linkText: {
    textAlign: 'center',
    color: COLOR_BLUE,
    fontSize: mainRegular,
    fontFamily: FONT_MEDIUM,
    letterSpacing: -0.41
  },
  link: {
    position: 'relative',
    width: '100%',
    borderRadius: scale(10)
  },
})
