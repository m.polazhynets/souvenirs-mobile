import { StyleSheet, Platform } from 'react-native';
import { COLOR_WHITE } from '../../../constants/Colors';
import variables, { v_scale, scale, FONT_BOLD_DISPLAY, FONT_REGULAR_DISPLAY, FONT_SEMIBOLD_DISPLAY, FONT_BOLD } from '../../../constants/StylesConstants';

const { xLarge, regular } = variables.fontSize;

export default StyleSheet.create({
  image: {
    backgroundColor: COLOR_WHITE,
    width: '100%',
    flex: 1,
    justifyContent: 'flex-end' 
  },
  card: {
    height: v_scale(220),
    marginBottom: v_scale(16),
    borderRadius: scale(10),
    overflow: 'hidden'
  },
  contentCard: {
    padding: scale(16),
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY, 
    fontSize: xLarge,
    letterSpacing: 0.35,
    color: COLOR_WHITE,
    marginVertical: v_scale(4)
  },
  text: {
    fontFamily: FONT_SEMIBOLD_DISPLAY,
    fontSize: regular,
    letterSpacing: 0.35,
    color: COLOR_WHITE
  },
  accentText: {
    fontFamily: FONT_BOLD,
    fontSize: regular,
    letterSpacing: -0.24,
    color: COLOR_WHITE,
    opacity: 0.6
  }
})

