import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, View, ImageBackground } from 'react-native';
import styles from './styles';

export default Card = ({
  onPress,
  title,
  accentText,
  descriprion,
  source,
  contentContainerStyle
}) => {


  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[styles.card, contentContainerStyle]}
    >
      <ImageBackground source={source} style={styles.image}>
        <View style={styles.contentCard}>
          <Text style={styles.accentText}>{accentText.toUpperCase()}</Text>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.text}>{descriprion}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
}

Card.propTypes = {
  // onPress: PropTypes.func,
  // comments: PropTypes.string,
  // title: PropTypes.string,
  // contentContainerStyle: PropTypes.object
};