import React from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-native-switch-pro';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import { scale } from '../../constants/StylesConstants';
import { COLOR_WHITE, COLOR_BLUE } from '../../constants/Colors';
import styles from './styles';

export default SwitchItem = ({
  onPress,
  title,
  value,
  name
}) => {


  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => onPress({ value: !value, name })}
      style={styles.item}
    >
      <View style={styles.textWrapper}>
        <Text style={styles.itemText}>{title}</Text>
      </View>
      <View style={styles.rightContent}>
        <Switch
          value={value}
          onSyncPress={(value) => onPress({ value, name })}
          style={styles.switchStyle}
          circleStyle={styles.circleStyle}
          height={scale(31)}
          width={scale(52)}
          circleColorActive={COLOR_WHITE}
          backgroundActive={COLOR_BLUE}
        />
      </View>
    </TouchableOpacity>
  );
}

SwitchItem.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  value: PropTypes.bool,
  name: PropTypes.string,
};