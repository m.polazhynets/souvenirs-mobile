import { StyleSheet } from 'react-native';
import { COLOR_BLACK } from '../../../constants/Colors';
import variables, { scale, FONT_REGULAR } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  itemText: {
    color: COLOR_BLACK,
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    letterSpacing: -0.41
  },
  item: {
    flexDirection: 'row',
    paddingVertical: scale(12),
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textWrapper: {
    justifyContent: 'center',
    width: '80%'
  },
  rightContent: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  switchStyle: {
    overflow: 'visible',
  },
  circleStyle: {
    height: scale(28),
    width: scale(28),
    borderRadius: scale(18),
  },
})
