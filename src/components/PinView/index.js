import React, { useEffect, useState } from "react"
import { Text, TouchableOpacity, View, ViewPropTypes } from "react-native"
import PropTypes from "prop-types"
import PinViewStyle from "./PinViewStyle.js"

const ViewButton = ({
  activeOpacity,
  onButtonPress,
  buttonSize = 60,
  text,
  description,
  customComponent,
  customViewStyle,
  accessible,
  accessibilityLabel,
  disabled,
  customTextStyle,
  customDescrStyle
}) => {

  return (
    <TouchableOpacity
      accessible={accessible}
      accessibilityRole="keyboardkey"
      accessibilityLabel={customComponent !== undefined ? accessibilityLabel : text}
      activeOpacity={activeOpacity}
      disabled={disabled}
      style={PinViewStyle.buttonContainer}
      onPress={onButtonPress}>
      <View
        style={[
          PinViewStyle.buttonView,
          customViewStyle,
          { width: buttonSize, height: buttonSize, borderRadius: buttonSize / 2 },
        ]}>
        {customComponent !== undefined ? (
          customComponent
        ) : (
          <React.Fragment>
            <Text style={[PinViewStyle.buttonText, customTextStyle]}>{text}</Text>
            {description && <Text style={[PinViewStyle.buttonTextDescr, customDescrStyle]}>{description}</Text>}
          </React.Fragment>
        )}
      </View>
    </TouchableOpacity>
  )
}

const ViewInput = ({
  showInputText = false,
  inputTextStyle,
  size = 40,
  customStyle,
  text,
  inputFilledStyle = { backgroundColor: "#000" },
  inputEmptyStyle = { backgroundColor: "#FFF" },
}) => {
  if (showInputText) {
    return (
      <View
        style={[
          PinViewStyle.inputView,
          customStyle,
          { width: size, height: size, borderRadius: size / 2, alignItems: "center", justifyContent: "center" },
          text !== undefined ? inputFilledStyle : inputEmptyStyle,
        ]}>
        <Text style={[PinViewStyle.inputText, inputTextStyle]}>{text}</Text>
      </View>
    )
  } else {
    return (
      <View
        style={[
          PinViewStyle.inputView,
          customStyle,
          { width: size, height: size, borderRadius: size / 2 },
          text !== undefined ? inputFilledStyle : inputEmptyStyle,
        ]}
      />
    )
  }
}

const ViewHolder = () => {
  return <View style={PinViewStyle.buttonContainer} />
}

const PinView = React.forwardRef(
  (
    {
      buttonTextByKey,
      accessible,
      style,
      onButtonPress,
      onValueChange,
      buttonAreaStyle,
      inputAreaStyle,
      inputViewStyle,
      activeOpacity,
      pinLength,
      buttonSize,
      buttonViewStyle,
      buttonTextStyle ,
      buttonDescrStyle,
      inputViewEmptyStyle,
      inputViewFilledStyle,
      showInputText,
      inputTextStyle,
      inputSize,
      disabled,

      customLeftButton,
      customRightButton,
      customRightAccessibilityLabel,
      customLeftAccessibilityLabel,
      customLeftButtonViewStyle,
      customRightButtonViewStyle,
      customLeftButtonDisabled,
      customRightButtonDisabled,
    },
    ref
  ) => {
    const [input, setInput] = useState("")
    ref.current = {
      clear: () => {
        if (input.length > 0) {
          setInput(input.slice(0, -1))
        }
      },
      clearAll: () => {
        if (input.length > 0) {
          setInput("")
        }
      },
    }

    const onButtonPressHandle = (key, value) => {
      onButtonPress(key)
      if (input.length < pinLength) {
        setInput(input + "" + value)
      }
    }

    useEffect(() => {
      if (onValueChange!==undefined){
         onValueChange(input)
      }
    }, [input])

    return (
      <View style={[PinViewStyle.pinView, style]}>
        <View style={[PinViewStyle.inputContainer, inputAreaStyle]}>
          {Array.apply(null, { length: pinLength }).map((e, i) => (
            <ViewInput
              inputTextStyle={inputTextStyle}
              showInputText={showInputText}
              inputEmptyStyle={inputViewEmptyStyle}
              inputFilledStyle={inputViewFilledStyle}
              text={input[i]}
              customStyle={inputViewStyle}
              size={inputSize}
              key={"input-view-" + i}
            />
          ))}
        </View>
        <View style={[PinViewStyle.buttonAreaContainer, buttonAreaStyle]}>
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("one", "1")}
            buttonSize={buttonSize}
            text={buttonTextByKey.one.text}
            description={buttonTextByKey.one.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("two", "2")}
            buttonSize={buttonSize}
            text={buttonTextByKey.two.text}
            description={buttonTextByKey.two.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("three", "3")}
            buttonSize={buttonSize}
            text={buttonTextByKey.three.text}
            description={buttonTextByKey.three.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("four", "4")}
            buttonSize={buttonSize}
            text={buttonTextByKey.four.text}
            description={buttonTextByKey.four.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("five", "5")}
            buttonSize={buttonSize}
            text={buttonTextByKey.five.text}
            description={buttonTextByKey.five.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("six", "6")}
            buttonSize={buttonSize}
            text={buttonTextByKey.six.text}
            description={buttonTextByKey.six.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("seven", "7")}
            buttonSize={buttonSize}
            text={buttonTextByKey.seven.text}
            description={buttonTextByKey.seven.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("eight", "8")}
            buttonSize={buttonSize}
            text={buttonTextByKey.eight.text}
            description={buttonTextByKey.eight.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("nine", "9")}
            buttonSize={buttonSize}
            text={buttonTextByKey.nine.text}
            description={buttonTextByKey.nine.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          {customLeftButton !== undefined ? (
            <ViewButton
              disabled={customLeftButtonDisabled}
              accessible={accessible}
              activeOpacity={activeOpacity}
              accessibilityLabel={customLeftAccessibilityLabel}
              onButtonPress={() => onButtonPress("custom_left")}
              customViewStyle={customLeftButtonViewStyle}
              customComponent={customLeftButton}
            />
          ) : (
            <ViewHolder />
          )}
          <ViewButton
            disabled={disabled}
            accessible={accessible}
            activeOpacity={activeOpacity}
            onButtonPress={() => onButtonPressHandle("zero", "0")}
            buttonSize={buttonSize}
            text={buttonTextByKey.zero.text}
            description={buttonTextByKey.zero.descr}
            customTextStyle={buttonTextStyle}
            customDescrStyle={buttonDescrStyle}
            customViewStyle={buttonViewStyle}
          />
          {customRightButton !== undefined ? (
            <ViewButton
              disabled={customRightButtonDisabled}
              accessible={accessible}
              activeOpacity={activeOpacity}
              accessibilityLabel={customRightAccessibilityLabel}
              onButtonPress={() => onButtonPress("custom_right")}
              customViewStyle={customRightButtonViewStyle}
              customComponent={customRightButton}
            />
          ) : (
            <ViewHolder />
          )}
        </View>
      </View>
    )
  }
)

PinView.defaultProps = {
  buttonTextByKey: {
    one: {
      text: "1",
      descr: ' '
    },
    two: {
      text: "2",
      descr: 'ABC'
    },
    three: {
      text: "3",
      descr: 'DEF'
    },
    four: {
      text: "4",
      descr: 'GHI'
    },
    five: {
      text: "5",
      descr: 'JKL'
    },
    six: {
      text: "6",
      descr: 'MNO'
    },
    seven: {
      text: "7",
      descr: 'PQRS'
    },
    eight: {
      text: "8",
      descr: 'TUV'
    },
    nine: {
      text: "9",
      descr: 'WXYZ'
    },
    zero: {
      text: "0",
      descr: null
    },
  },
  accessible: false,
  onButtonPress: () => {},
  inputTextStyle : { color: "#FFF" },
  buttonAreaStyle : { marginVertical: 12 },
  inputAreaStyle : { marginVertical: 12 },
  activeOpacity :0.9,
  buttonTextStyle : { color: "#FFF", fontSize: 30 },
  customRightAccessibilityLabel : "right",
  customLeftAccessibilityLabel : "left",
  disabled: false,
  customLeftButtonDisabled: false,
  customRightButtonDisabled: false,
}
PinView.propTypes = {
  pinLength: PropTypes.number.isRequired,

  accessible: PropTypes.bool,
  style : ViewPropTypes.style,
  onButtonPress: PropTypes.func,
  onValueChange: PropTypes.func,
  showInputText: PropTypes.bool,

  inputAreaStyle: ViewPropTypes.style,
  inputViewStyle: ViewPropTypes.style,
  inputTextStyle: Text.propTypes.style,
  inputViewEmptyStyle: ViewPropTypes.style,
  inputSize:PropTypes.number,
  inputViewFilledStyle: ViewPropTypes.style,

  customRightAccessibilityLabel: PropTypes.string,
  customRightButtonDisabled: PropTypes.bool,
  customRightButtonViewStyle: ViewPropTypes.style,
  customRightButton: PropTypes.element,

  customLeftAccessibilityLabel: PropTypes.string,
  customLeftButtonDisabled: PropTypes.bool,
  customLeftButtonViewStyle: ViewPropTypes.style,
  customLeftButton: PropTypes.element,

  buttonTextByKey : PropTypes.object,
  buttonAreaStyle : ViewPropTypes.style,
  activeOpacity:PropTypes.number,
  buttonSize:PropTypes.number,
  buttonViewStyle: ViewPropTypes.style,
  buttonTextStyle: Text.propTypes.style,
  buttonDescrStyle: Text.propTypes.style,
  disabled: PropTypes.bool,
}

export default PinView
