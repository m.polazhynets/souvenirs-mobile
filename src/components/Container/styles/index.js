import { StyleSheet, Platform } from 'react-native';
import { COLOR_WHITE } from '../../../constants/Colors';
import { deviceHeight } from '../../../constants/StylesConstants';

export default StyleSheet.create({
  content: {
    backgroundColor: 'transparent',
    flexGrow: 1,
  },
  background: {
    width: '100%',
    backgroundColor: COLOR_WHITE,
    height: deviceHeight,
    flex: 1,
  },
})

