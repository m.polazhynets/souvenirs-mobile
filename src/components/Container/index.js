import React from 'react';
import PropTypes from 'prop-types';
import { ImageBackground, StatusBar } from 'react-native';
import { SafeAreaView, useSafeArea } from 'react-native-safe-area-context';
import { useRoute } from '@react-navigation/native';
import { BACKGROUND_IMAGE } from '../../constants/Images';
import { COLOR_WHITE } from '../../constants/Colors';

import styles from './styles';
import { HomeFlow, SaleFlow, QRCodeFlow, ProfileFlow, MenuFlow } from '../../navigator/Keys';

const LIST_HIDE_BOTTOM_SAVE_AREA = [HomeFlow.Home, SaleFlow.Sale, QRCodeFlow.QRCode, ProfileFlow.Profile, MenuFlow.Menu]

export default ContainerComponent = ({ children, hideBackground }) => {
  const insets = useSafeArea();
  const route = useRoute();
  const paddingBottom = LIST_HIDE_BOTTOM_SAVE_AREA.includes(route.name) ? 0 : insets.bottom;

  return (
    <ImageBackground source={BACKGROUND_IMAGE} style={styles.background}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={COLOR_WHITE}
      />
      <SafeAreaView style={[styles.content, (hideBackground) && { backgroundColor: COLOR_WHITE }, {paddingBottom}]} >
        {children}
      </SafeAreaView>
    </ImageBackground>
  );
}

ContainerComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  hideBackground: PropTypes.bool
};