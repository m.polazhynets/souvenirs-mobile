import { StyleSheet } from 'react-native';
import { scale, v_scale } from '../../../constants/StylesConstants';

export default StyleSheet.create({
  arrowIcon: {
    width: scale(12),
    height: v_scale(20)
  },
  backButton: {
    padding: scale(9),
    zIndex: 1
  },
  headerWrapper: {
    height: v_scale(50),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: scale(16)
  },
  logo: {
    height: v_scale(35)
  },
  logoWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
})