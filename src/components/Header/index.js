import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ARROW_ICON, ARROW_WHITE_ICON, LOGO_ICON } from '../../constants/Images';
import styles from './styles';

export default Header = ({ hideLogo, hideBackArrow, rightBtn, headerStyle={}, whiteBackBtn }) => {
  const navigation = useNavigation();
  return (
    <View style={[styles.headerWrapper, headerStyle]}>
      {
        !hideBackArrow && (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.backButton}
            onPress={() => navigation.goBack()}
          >
            <Image
              source={whiteBackBtn ? ARROW_WHITE_ICON : ARROW_ICON}
              style={styles.arrowIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )
      }
      {
        !hideLogo && (
          <View style={styles.logoWrapper}>
            <Image
              source={LOGO_ICON}
              style={styles.logo}
              resizeMode='contain'
            />
          </View>
        )
      }
      {
        rightBtn
      }
    </View>
  );
}

Header.propTypes = {
  hideLogo: PropTypes.bool,
  hideBackArrow: PropTypes.bool,
  rightBtn: PropTypes.node,
  headerStyle: PropTypes.oneOfType([
    PropTypes.styles,
    PropTypes.object
  ]),
  whiteBackBtn: PropTypes.bool
};