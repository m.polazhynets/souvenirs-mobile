import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import { ARROW_RIGHT_GRAY_ICON } from '../../constants/Images';
import styles from './styles';

export default MenuItem = ({
  onPress,
  title,
  comments,
  contentContainerStyle
}) => {


  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[styles.item, contentContainerStyle]}
    >
      <View style={styles.textWrapper}>
        <Text style={styles.itemText}>{title}</Text>
      </View>
      <View style={styles.rightContent}>
        <Text style={styles.comment}>{comments}</Text>
        <Image
          source={ARROW_RIGHT_GRAY_ICON}
          style={styles.icon}
          resizeMode="contain"
        />
      </View>
    </TouchableOpacity>
  );
}

MenuItem.propTypes = {
  onPress: PropTypes.func,
  comments: PropTypes.string,
  title: PropTypes.string,
  contentContainerStyle: PropTypes.object
};