import { StyleSheet } from 'react-native';
import { COLOR_BLACK } from '../../../constants/Colors';
import variables, { scale, FONT_REGULAR } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  itemText: {
    color: COLOR_BLACK,
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    letterSpacing: -0.41
  },
  icon: {
    width: scale(8),
    height: scale(14)
  },
  item: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    paddingVertical: scale(16),
    paddingRight: scale(16),
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textWrapper: {
    justifyContent: 'center'
  },
  rightContent: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  comment: {
    color: COLOR_BLACK,
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    letterSpacing: -0.41,
    opacity: 0.4,
    marginRight: scale(7)
  }
})
