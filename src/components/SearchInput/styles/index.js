import { StyleSheet, Platform } from 'react-native';
import { COLOR_BLACK } from '../../../constants/Colors';
import variables, { FONT_REGULAR, v_scale, scale } from '../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;


export default StyleSheet.create({
 
  inputStyles: {
    fontFamily: FONT_REGULAR,
    color: 'rgba(0, 0, 0, 0.4)'
  },
  inputContainerStyle: {
    backgroundColor: 'rgba(142, 142, 147, 0.12)',
    borderColor: 'rgba(142, 142, 147, 0.12)',
    borderRadius: scale(10),
    paddingHorizontal: scale(30)
  },
  searchIcon: {
    width: scale(14),
    height: scale(14),
    position: 'absolute',
    left: scale(10),
    zIndex: 1
  },
  dictationIcon: {
    width: scale(12),
    height: scale(19),
    position: 'absolute',
    right: scale(10),
    zIndex: 1
  },
  container: {
    position: 'relative',
    justifyContent: 'center'
  }
});
