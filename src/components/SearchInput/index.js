import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image } from 'react-native';
import { Input } from '../index';
import { SEARCH_ICON, DICTATION_ICON } from '../../constants/Images';
import styles from './styles';

class SearchInput extends Component {
  render() {
    const { inputProps } = this.props;

    return (
      <View style={styles.container}>
        <Image
          source={SEARCH_ICON}
          style={styles.searchIcon}
          resizeMode="contain"
        />
        <Input
          {...inputProps}
          inputStyles={styles.inputStyles}
          containerStyles={styles.inputContainerStyle}
        />
        <Image
          source={DICTATION_ICON}
          style={styles.dictationIcon}
          resizeMode="contain"
        />
      </View>
    )
  }
}

export default SearchInput;

SearchInput.propTypes = {
  inputProps: PropTypes.object
};