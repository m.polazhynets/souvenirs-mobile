import { StyleSheet, Platform } from 'react-native';
import { getStatusBarHeight, isIphoneX } from 'react-native-iphone-x-helper'
import { COLOR_BLACK, COLOR_BLUE, COLOR_WHITE, COLOR_GRAY_5 } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, deviceWidth } from '../../../../constants/StylesConstants';

const { large, extLarge, small, mainRegular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    fontSize: extLarge,
    letterSpacing: 0.34
  },
  content: {
    padding: scale(16),
    paddingBottom: 0,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK
  },
  sliderWrapper: {
    height: v_scale(375)
  },
  image: {
    width: deviceWidth,
    height: v_scale(375),
    backgroundColor: 'rgba(0, 0, 0, 0.3)'
  },
  headerStyle: {
    position: 'absolute',
    width: '100%',
    zIndex: 1, 
    top: (Platform.OS === 'ios' && isIphoneX()) ? getStatusBarHeight() + v_scale(16) : (Platform.OS === 'ios' ) ? getStatusBarHeight() :  0,
  },
  dotsContainerStyle: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    paddingVertical: v_scale(18),
  },
  dotStyle: {
    width: scale(8),
    height: scale(8),
    borderRadius: scale(8),
    backgroundColor: COLOR_BLUE,
    marginHorizontal: -scale(5)
  },
  inactiveDotStyle: {
    backgroundColor: COLOR_WHITE,
    opacity: 0.2,
    width: scale(8),
    height: scale(8),
  },
  icon: {
    width: scale(24),
    height: scale(24)
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: -scale(4),
    marginTop: v_scale(10),
    marginBottom: v_scale(25)
  },
  button: {
    backgroundColor: COLOR_GRAY_5,
    borderRadius: scale(10),
    padding: scale(6),
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: scale(4)
  },
  buttonText: {
    color: COLOR_BLUE,
    fontFamily: FONT_REGULAR,
    fontSize: small,
    letterSpacing: 0.07
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(14),
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  itemTitle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    letterSpacing: -0.41
  },
  itemValue: {
    opacity: 0.4
  },
  noBorder: {
    borderBottomWidth: 0
  },
  list: {
    paddingTop: v_scale(14),
  },
  bottomImage: {
    width: scale(93),
    height: v_scale(130)
  },
  bottomImageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: v_scale(50)
  }
})