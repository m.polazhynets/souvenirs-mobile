import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Container, Header } from '../../../components';
import { deviceWidth } from '../../../constants/StylesConstants';
import { CALL_ICON, MAP_ICON, FULL_LOGO_ICON } from "../../../constants/Images";
import styles from './styles';


const DEMO = [0, 1, 3]

class AboutScreen extends Component {
  state = {
    activeSlide: 0
  }
  _carousel = React.createRef()

  _renderItem = ({ item, index }) => {
    return (
      <View style={styles.slide}>
        <Image style={styles.image} />
      </View>
    );
  }

  pagination = () => {
    const { activeSlide } = this.state;

    return (
      <Pagination
        dotsLength={DEMO.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.dotsContainerStyle}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    );
  }

  renderSlider = () => {
    return (
      <View style={styles.sliderWrapper}>
        <Carousel
          ref={this._carousel}
          data={DEMO}
          renderItem={this._renderItem}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          onSnapToItem={(index) => this.setState({ activeSlide: index })}
        />
        {this.pagination()}
      </View>
    )
  }

  renderListItem = (title, value, onPress, noBorder) => {
    return (
      <TouchableOpacity
        activeOpacity={onPress ? 0.8 : 1}
        onPress={() => {
          if (onPress) onPress();
        }}
        style={[styles.listItem, noBorder && styles.noBorder]}
      >
        <Text style={styles.itemTitle}>{title}</Text>
        <Text style={[styles.itemTitle, styles.itemValue]}>{value}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header
          hideLogo
          whiteBackBtn
          headerStyle={styles.headerStyle}
        />
        <ScrollView style={styles.container}>
          {this.renderSlider()}
          <View style={styles.content}>
            <Text style={styles.title}>{t('menu_flow:name_of_app')}</Text>
            <View style={styles.buttons}>
              <TouchableOpacity
                style={styles.button}
                activeOpacity={0.8}
                onPress={() => {}}
              >
                <Image 
                  source={CALL_ICON} 
                  style={styles.icon} 
                  resizeMode="contain"
                />
                <Text style={styles.buttonText}>{t('common:buttons.call')}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                activeOpacity={0.8}
                onPress={() => {}}
              >
                <Image
                  source={MAP_ICON} 
                  style={styles.icon} 
                  resizeMode="contain"
                />
                <Text style={styles.buttonText}>{t('common:buttons.route')}</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.text}>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.</Text>
            <View style={styles.list}>
              {this.renderListItem(t('menu_flow:time_to_work'), '10:00 – 22:00')}
              {this.renderListItem(t('menu_flow:phone_number'), '+49 30 2639190', () => {})}
              {this.renderListItem(t('menu_flow:web_site'), 'greenplace.com', () => {}, true)}
            </View>
          </View>
          <View style={styles.bottomImageWrapper}> 
              <Image 
                source={FULL_LOGO_ICON}
                resizeMode="contain"
                style={styles.bottomImage}
              />
            </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'menu_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(AboutScreen),
);