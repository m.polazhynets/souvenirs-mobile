import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header } from '../../../components';
import styles from './styles';


class LoyaltyRulesScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo/>
        <Text style={styles.title}>{t('menu_flow:loyalty_system_rules')}</Text>
        <ScrollView style={styles.container}>
         
          <View style={styles.content}>
            <Text style={styles.text}>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.</Text>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'menu_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(LoyaltyRulesScreen),
);