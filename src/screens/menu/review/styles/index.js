import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR } from '../../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0,
  },
  content: {
    paddingTop: v_scale(9)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    letterSpacing: -0.41,
    color: COLOR_BLACK
  },
  buttonWrapper: {
    marginTop: v_scale(25)
  },
  inputStyles: {
    height: v_scale(145),
    textAlignVertical: 'top'
  },
  inputContainerStyles: {
    marginTop: v_scale(65)
  }
})