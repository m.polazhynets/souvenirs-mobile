import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Container, Header, Button, Input } from '../../../components';
import styles from './styles';


class ReviewScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('menu_flow:leave_review')}</Text>
        <KeyboardAwareScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          <View style={styles.content}>
            <Text style={styles.text}>{t('menu_flow:review_description')}</Text>
            <Input
              placeholder={t('common:form:placeholders.write_your_message')}
              inputStyles={styles.inputStyles}
              containerStyles={styles.inputContainerStyles}
              value={null}
              required
              isValid={null}
              onBlur={() => { }}
              onChangeText={(val) => { }}
              multiline
              noMargin
            />
            <Button
              title={t('common:buttons.send')}
              contentContainerStyle={styles.buttonWrapper}
              onPress={() => {}} 
              type="primary"
              isLoading={false}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'menu_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(ReviewScreen),
);