import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Container, Header, SearchInput } from '../../../components';
import { ARROW_BOTTOM_GRAY_ICON, ARROW_TOP_BLUE_ICON } from '../../../constants/Images';
import styles from './styles';

const DEMO = [
  {
    id: 0,
    title: 'Вопрос первый',
    content: 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.',
  },{
    id: 1,
    title: 'Вопрос первый',
    content: 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.',
  },{
    id: 2,
    title: 'Вопрос первый',
    content: 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.',
  }
]

class FAQsScreen extends Component {
  state = {
    activeSections: [],
  };

  
  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  _renderHeader = (section, index, isActive, sections) => {
    return (
      <Animatable.View style={[styles.headerList, (isActive) && styles.hideBorder]}>
        <Text style={styles.headerText}>{section.title}</Text>
        <Animatable.Image
          duration={300}
          transition="rotate"
          style={styles.icon}
          resizeMode={'contain'}
          source={isActive ? ARROW_TOP_BLUE_ICON : ARROW_BOTTOM_GRAY_ICON}
        />
      </Animatable.View>
    );
  };

  _renderContent = section => {
    return (
      <View style={styles.contentItem}>
        <Text style={styles.contentText}>{section.content}</Text>
      </View>
    );
  };

  render() {
    const { t } = this.props;
    const { activeSections } = this.state;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('menu_flow:faq')}</Text>
        <KeyboardAwareScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          
          <SearchInput
            inputProps={{
              placeholder: t('common:form:placeholders.search'),
              value:null,
              onBlur:() => { },
              onChangeText:(val) => { },
              noMargin: true
            }}
          />
          <View style={styles.content}>
            <Accordion
              sections={DEMO}
              touchableComponent={TouchableOpacity}
              touchableProps={{ activeOpacity: 0.8 }}
              activeSections={activeSections}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'menu_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(FAQsScreen),
);