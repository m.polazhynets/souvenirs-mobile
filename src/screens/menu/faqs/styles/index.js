import { StyleSheet } from 'react-native';
import { COLOR_RED, COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_BOLD } from '../../../../constants/StylesConstants';

const { mainRegular, large } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: v_scale(9)
  },
  content: {
    paddingTop: v_scale(25)
  },
  contentText: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK,
  },
  headerList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(18),
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)'
  },
  headerText: {
    fontFamily: FONT_BOLD,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK,
  },
  contentItem: {
    paddingBottom: v_scale(18),
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)'
  },
  icon: {
    width: scale(13),
    height: scale(8)
  },
  hideBorder: {
    borderBottomWidth: 0
  }
})