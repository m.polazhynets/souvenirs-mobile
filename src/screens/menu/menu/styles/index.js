import { StyleSheet } from 'react-native';
import { COLOR_RED  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR } from '../../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0,
  },
  menuList: {
    paddingTop: v_scale(4),
    marginBottom: v_scale(20)
  },
  bottomMenuList: {
    paddingLeft: scale(16)
  },
  signOutText: {
    color: COLOR_RED,
    fontSize: mainRegular,
    fontFamily: FONT_REGULAR,
    letterSpacing: -0.41
  },
  signOut: {
    paddingVertical: v_scale(40)
  }
})