import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import i18n from '../../../utils/i18n';
import { setIsAuthorized } from '../../../actions/authActions';
import { Container, Header, ListItem } from '../../../components';
import MenuItem from './components/menuItem';
import { replace, navigate } from '../../../navigator/RootNavigation';
import { InitFlow, FlowKeys, MenuFlow } from '../../../navigator/Keys';
import { FILE_TEXT_INFO_ICON, FILE_TEXT_ICON, HELP_CIRCLE_OUTLINE_ICON, MESSAGE_ICON, SHARE_ICON } from '../../../constants/Images';
import styles from './styles';

const MENU_LIST = [
  {
    key: '1',
    title: i18n.t('menu_flow:about_shop'),
    source: FILE_TEXT_INFO_ICON,
    navigate: MenuFlow.About
  }, {
    key: '2',
    title: i18n.t('menu_flow:share_code'),
    source: SHARE_ICON
  }, {
    key: '3',
    title: i18n.t('menu_flow:loyalty_system_rules'),
    source: FILE_TEXT_ICON,
    navigate: MenuFlow.LoyaltyRules
  }, {
    key: '4',
    title: i18n.t('menu_flow:faq'),
    source: HELP_CIRCLE_OUTLINE_ICON,
    navigate: MenuFlow.FAQs
  }, {
    key: '5',
    title: i18n.t('menu_flow:share_app'),
    source: MESSAGE_ICON
  },
]
const BOTTOM_MENU_LIST = [
  {
    key: '1',
    title: i18n.t('menu_flow:privacy_policy'),
    navigate: MenuFlow.PrivacyPolicy
  }, {
    key: '2',
    title: i18n.t('menu_flow:terms_of_use'),
    navigate: MenuFlow.TermsOfUse
  }, {
    key: '3',
    title: i18n.t('menu_flow:send_comment'),
    navigate: MenuFlow.Review
  }
]

class MenuScreen extends Component {

  signOut = () => {
    const { setIsAuthorized } = this.props;

    setIsAuthorized(false);
    replace(FlowKeys.Initial, { screen: InitFlow.LogIn })
  }

  onNavigate = (item) => {
    if (!item.navigate) return;

    navigate(item.navigate)
  }

  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header hideBackArrow  hideLogo/>
        <Text style={styles.title}>{t('menu_flow:menu')}</Text>
        <ScrollView >
          <View style={styles.container}>
            <View style={styles.menuList}>
              {MENU_LIST.map(item => <MenuItem {...item} onPress={() => this.onNavigate(item)} />)}
            </View>
          </View>
          <View style={styles.bottomMenuList}>
            {BOTTOM_MENU_LIST.map(item => <ListItem {...item} onPress={() => this.onNavigate(item)} />)}
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={this.signOut}
              style={styles.signOut}
            >
              <Text style={styles.signOutText}>{t('menu_flow:exit_application')}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIsAuthorized
    },
    dispatch,
  );

export default withTranslation(['common', 'menu_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(MenuScreen),
);