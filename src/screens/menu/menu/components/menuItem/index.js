import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import styles from './styles';

export default MenuItem = ({
  onPress,
  source,
  title,
}) => {


  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={styles.item}
    >
      <View style={styles.imageWrapper}>
        <Image
          source={source}
          style={styles.icon}
          resizeMode="contain"
        />
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.itemText}>{title}</Text>
      </View>

    </TouchableOpacity>
  );
}

MenuItem.propTypes = {
  onPress: PropTypes.func,
  source: PropTypes.number,
  title: PropTypes.string,
};