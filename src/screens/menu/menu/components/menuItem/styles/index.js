import { StyleSheet } from 'react-native';
import { COLOR_BLUE, COLOR_BLACK } from '../../../../../../constants/Colors';
import variables, { scale, FONT_MEDIUM, v_scale, FONT_BOLD } from '../../../../../../constants/StylesConstants';

const { mainRegular } = variables.fontSize;

export default StyleSheet.create({
  itemText: {
    color: COLOR_BLACK,
    fontSize: mainRegular,
    fontFamily: FONT_MEDIUM,
    letterSpacing: -0.41
  },
  icon: {
    width: scale(24),
    height: scale(24)
  },
  imageWrapper: {
    width: scale(40),
    height: scale(40),
    backgroundColor: 'rgba(57, 121, 229, 0.1)',
    borderRadius: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(12),
    marginVertical: v_scale(8)
  },
  item: {
    flexDirection: 'row',
  },
  textWrapper: {
    borderBottomWidth: 0.5,
    width: '100%',
    borderColor: 'rgba(0, 0, 0, 0.2)',
  
    justifyContent: 'center'
  }
})
