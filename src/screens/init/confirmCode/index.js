import React, { Component } from 'react';
import { View, Text, ScrollView, TextInput, Keyboard, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header, ButtonLink } from '../../../components';
import { replace } from '../../../navigator/RootNavigation';
import { InitFlow } from '../../../navigator/Keys';
import styles from './styles';

class ConfirmCodeScreen extends Component {
  state = {
    code: ''
  }
  input = React.createRef();

  componentWillUnmount() {
    Keyboard.dismiss()
  }

  onChangeText = (val) => {
    this.setState({ code: val }, () => {
      if (val.length === 4) {
        this.setState({ code: '' })
        replace(InitFlow.SignUpPrivateData)
      }
    })
  }

  render() {
    const { t } = this.props;
    const { code } = this.state;

    return (
      <Container>
        <Header />
        <ScrollView contentContainerStyle={styles.container} >
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:write_sms_code')}</Text>
            <Text style={styles.description}>{t('auth_flow:we_sent_sms')}</Text>
            <Text style={styles.description}>+7 (917) 614-3054</Text>
          </View>
          <TouchableOpacity 
            style={styles.circleBlock} 
            onPress={() => this.input.current.focus()} 
            activeOpacity={0.9}
          >
            <View style={[styles.circle, (code.length < 1 ) && styles.in_active]}/>
            <View style={[styles.circle, (code.length < 2 ) && styles.in_active]}/>
            <View style={[styles.circle, (code.length < 3 ) && styles.in_active]}/>
            <View style={[styles.circle, (code.length < 4 ) && styles.in_active]}/>
          </TouchableOpacity>
          <TextInput
            ref={this.input} 
            value={code}
            keyboardType={'numeric'}
            maxLength={4}
            autoFocus
            onChangeText={this.onChangeText}
            style={styles.hideInput}
          />
          <View style={[styles.linkBlock, styles.disableStyle]}>
            <ButtonLink
              title={t('common:buttons.send_new_sms')}
              onPress={() => {}}
            />
            <Text style={[styles.description, styles.link_description]}>{`${t('auth_flow:will_be_available')} 8 ${t('auth_flow:seconds')}`}</Text>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(ConfirmCodeScreen),
);