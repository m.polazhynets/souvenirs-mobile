import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1, COLOR_BLACK, COLOR_BLUE, COLOR_RED } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1,
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: v_scale(15),
    paddingHorizontal: scale(20)
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  description: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    textAlign: 'center',
    color: COLOR_GRAY_1,
    letterSpacing:  -0.08,
  },
  link_description: {
    marginTop: v_scale(8),
    color: COLOR_BLACK
  },
  disableStyle: {
    opacity: 0.4
  },
  hideInput: {
    position: 'absolute',
    opacity: 0
  },
  circleBlock: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: v_scale(50)
  },
  circle: {
    width: scale(18),
    height: scale(18), 
    backgroundColor: COLOR_BLUE,
    borderRadius: scale(9),
    marginHorizontal: scale(12)
  },
  in_active: {
    opacity: 0.4
  },
  error: {
    opacity: 1,
    backgroundColor: COLOR_RED
  }
})