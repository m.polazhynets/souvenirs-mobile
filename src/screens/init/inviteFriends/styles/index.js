import { StyleSheet } from 'react-native';
import { COLOR_BLACK } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale, FONT_BOLD } from '../../../../constants/StylesConstants';

const { extLarge, mainRegular } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(20),
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  image: {
    width: scale(50),
    height: scale(50),
    marginBottom: v_scale(25)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    textAlign: 'center',
    color: COLOR_BLACK,
    letterSpacing:  -0.41,
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
  },
  headerButtonWrapper: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  headerButton: {
    width: 'auto'
  },
  bottomContent: {
    width: '100%'
  },
  copyText: {
    fontFamily: FONT_BOLD,
    fontSize: scale(24),
    letterSpacing: -0.41
  },
  copyBlock: {
    flexDirection: 'row', 
    height: v_scale(65),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'rgba(0, 0, 0, 0.24)',
    borderStyle: 'dashed',
    borderRadius: scale(10),
    marginBottom: v_scale(30),
    position: 'relative'
  },
  copyIcon: {
    position: 'absolute',
    right: scale(22),
    width: scale(24),
    height: scale(24)
  }
})