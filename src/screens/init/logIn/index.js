import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { setIsAuthorized } from '../../../actions/authActions';
import { Button, Input, ButtonLink, Container } from '../../../components';
import { navigate, replace } from '../../../navigator/RootNavigation';
import { InitFlow, HomeFlow, FlowKeys } from '../../../navigator/Keys';
import { FULL_LOGO_ICON } from '../../../constants/Images';
import styles from './styles';

class LogInScreen extends Component {
  renderRightInputLink = () => {
    const { t } = this.props;

    return (
      <View style={styles.inputRightlink}>
        <ButtonLink
          title={t('common:buttons.i_forgot')}
          onPress={() => navigate(InitFlow.NewPassword)}
        />
      </View>
    )
  }

  render() {
    const { t, setIsAuthorized, navigation } = this.props;

    return (
      <Container>
        <KeyboardAwareScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          <View style={styles.topContent}>
            <Image source={FULL_LOGO_ICON} style={styles.logo} resizeMode={'contain'} />
            <Text style={styles.title}>{t('auth_flow:login_to_app')}</Text>
            <Text style={styles.description}>{t('auth_flow:login_description')}</Text>
          </View>
          <Input
            placeholder={t('common:form:placeholders.email_or_phone')}
            value={null}
            required
            isValid={null}
            onBlur={() => { }}
            onChangeText={(val) => { }}
          />
          <Input
            placeholder={t('common:form:placeholders.write_password')}
            value={null}
            required
            secureTextEntry
            isValid={null}
            onBlur={() => { }}
            onChangeText={(val) => { }}
            rightContent={this.renderRightInputLink()}
          />
          <View style={styles.buttonsWrapper}>
            <Button
              title={t('common:buttons.come_in')}
              contentContainerStyle={styles.buttonWrapper}
              onPress={() => {
                setIsAuthorized(true);         
                replace(FlowKeys.Main, { screen: HomeFlow.Home })
              }} 
              type="primary"
              isLoading={false}
            />
            <Button
              title={t('common:buttons.i_am_new_user')}
              contentContainerStyle={styles.buttonWrapper}
              onPress={() => navigate(InitFlow.SignUp)}
              type="white"
              isLoading={false}
            />
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIsAuthorized
    },
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(LogInScreen),
);