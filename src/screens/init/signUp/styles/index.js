import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1, COLOR_BLUE } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall, regular } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1
  },
  buttonWrapper: {
    marginTop: v_scale(6)
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: v_scale(36),
    paddingHorizontal: scale(20)
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  description: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    textAlign: 'center',
    color: COLOR_GRAY_1,
    letterSpacing:  -0.08,
  },
  buttonsWrapper: {
    flex: 1,
    justifyContent: 'space-between'
  },
  link: {
    color: COLOR_BLUE,
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    letterSpacing: -0.41
  },
  textWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '70%'
  },
  switchStyle: {
    overflow: 'visible',
  },
  circleStyle: {
    height: scale(28),
    width: scale(28),
    borderRadius: scale(18),
  },
  bottomBlock: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
})