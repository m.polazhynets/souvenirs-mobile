import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import Switch from 'react-native-switch-pro';
import { Button, Input, Header, Container } from '../../../components';
import { navigate } from '../../../navigator/RootNavigation';
import { InitFlow } from '../../../navigator/Keys';
import { scale } from '../../../constants/StylesConstants';
import { COLOR_BLUE, COLOR_WHITE } from '../../../constants/Colors';
import styles from './styles';

class SignUpScreen extends Component {
  state = {
    conditions: true,
    form: {
      phone: {
        value: '',
        isValid: null,
      },
    }
  }

  validate = (val, name) => {
    const { form } = this.state;

    this.setState({
      form: {
        ...form,
        [name]: {
          ...form[name],
          value: val,
          isValid: (val.length === 18) ? true : false
        },
      }
    })
  }

  checkFullValidate = async () => {
    const { form: { phone } } = this.state;

    return new Promise((resolve, reject) => {
      this.setState({
        form: {
          ...this.state.form,
          phone: {
            ...phone,
            isValid: (phone.isValid === true)
          },
        }
      }, () => {
        const { form: { phone } } = this.state;

        (phone.isValid) ? resolve(true) : reject(false)
      })
    });
  }

  handleSubmit = async () => {
    const { form: { phone } } = this.state;

    try {
      await this.checkFullValidate();

      console.log(phone.value)
      navigate(InitFlow.ConfirmCode)
    } catch (err) {
      console.log('error validation')
    }
  };

  render() {
    const { t } = this.props;
    const { form: { phone }, conditions } = this.state;

    return (
      <Container>
        <Header />
        <KeyboardAwareScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:sign_up')}</Text>
            <Text style={styles.description}>{t('auth_flow:registration_descriprion')}</Text>
          </View>
          <Input
            mask={"+7 (999) 999 99 99"}
            maxLength={18}
            keyboardType={'numeric'}
            placeholder={t('common:form:placeholders.phone_number')}
            value={phone.value}
            required
            isValid={phone.isValid}
            onBlur={() => this.validate(phone.value, 'phone')}
            onChangeText={(val) => this.validate(val, 'phone')}
          />
          <View style={styles.buttonsWrapper}>
            <Button
              title={t('common:buttons.further')}
              contentContainerStyle={styles.buttonWrapper}
              onPress={this.handleSubmit}
              type="primary"
              isLoading={false}
              disable={!conditions}
            />
            <View style={styles.bottomBlock}>
              <View style={styles.textWrapper}>
                <Text style={styles.text}>{`${t('auth_flow:conditions_personal_data_1')}`}</Text>
                <TouchableOpacity
                  onPress={() => { }}
                  activeOpacity={0.8}
                >
                  <Text style={styles.text, styles.link}>{` ${t('auth_flow:conditions')} `}</Text>
                </TouchableOpacity>
                <Text style={styles.text}>{`${t('auth_flow:conditions_personal_data_2')}`}</Text>
              </View>
              <Switch
                value={conditions}
                onSyncPress={(value) => this.setState({conditions: value})}
                style={styles.switchStyle}
                circleStyle={styles.circleStyle}
                height={scale(31)}
                width={scale(52)}
                circleColorActive={COLOR_WHITE}
                backgroundActive={COLOR_BLUE}
              />
            </View>
          </View>

        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SignUpScreen),
);