import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { COLOR_WHITE } from '../../../constants/Colors';
import { navigate, replace } from '../../../navigator/RootNavigation';
import { InitFlow, FlowKeys, HomeFlow } from '../../../navigator/Keys';

class StartScreen extends Component {
  componentDidMount() {
    const { is_authorized, navigation } = this.props;

    setTimeout(() => {
      if (is_authorized) {
        navigation.replace(FlowKeys.Initial, { screen: InitFlow.Security })
      } else {
        navigation.replace(FlowKeys.Initial, { screen: InitFlow.LogIn })
      }
    }, 0)
  }
  
  render() {
    return (
      <StatusBar
        barStyle="dark-content"
        backgroundColor={COLOR_WHITE}
      />
    )
  }
}

const mapStateToProps = state => ({
  is_authorized: state.authReducer.is_authorized
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen)