import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { setIsAuthorized } from '../../../actions/authActions';
import { Button, Container, Header } from '../../../components';
import { navigate, reset, replace } from '../../../navigator/RootNavigation';
import { InitFlow, FlowKeys, HomeFlow } from '../../../navigator/Keys';
import styles from './styles';


class ActiveBonusProgramScreen extends Component {

  render() {
    const { t, setIsAuthorized } = this.props;

    return (
      <Container>
        <Header hideBackArrow hideLogo />
        <View style={styles.container}>
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:activate_bonus_program')}</Text>
          </View>
          <View style={styles.centerContent}>
            <Text style={styles.text}>{t('auth_flow:activate_bonus_description_1')}</Text>
            <Text style={styles.text}>{t('auth_flow:activate_bonus_description_2')}</Text>
          </View>
          <View style={styles.bottomContent}>
            <View style={styles.emptyBlock}/>
            <Button
              title={t('common:buttons.i_understood')}
              onPress={() => {
                setIsAuthorized(true);
                replace(FlowKeys.Main, { screen: HomeFlow.Home })
              }}
              type="primary"
              isLoading={false}
            />
          </View>
          
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIsAuthorized
    },
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(ActiveBonusProgramScreen),
);