import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { setIsAuthorized } from '../../../actions/authActions';
import { Container, Header, PinView, ButtonLink } from '../../../components';
import { replace } from '../../../navigator/RootNavigation';
import { InitFlow, FlowKeys, HomeFlow } from '../../../navigator/Keys';
import { CLOSE_SMALL_BLACK_ICON, FACE_SMALL_BLACK_ICON, USER_DEFAULT_IMAGE } from '../../../constants/Images';
import { scale } from '../../../constants/StylesConstants';
import styles from './styles';

class SecurityScreen extends Component {
  state = {
    createPin: this.props.route.params ? this.props.route.params.createPin : false,
    changePin: this.props.route.params ? this.props.route.params.changePin : false,
    enteredPin: "",
    showBiometricAuth: true,
    showRemoveButton: true
  }
  pinView = React.createRef();

  componentDidMount() {
    const { createPin } = this.state;
    if (createPin) {
      this.setState({ showBiometricAuth: false })
    }
  }

  setEnteredPin = (val) => {
    const { createPin } = this.state;

    this.setState({ enteredPin: val }, () => {
      if (val.length === 4) {
        if (createPin) {
          replace(InitFlow.EnableBiometric)
        } else {
          replace(FlowKeys.Main, { screen: HomeFlow.Home })
        }
      }
    })
  }

  logOut = () => {
    const { setIsAuthorized } = this.props;

    setIsAuthorized(false);
    replace(FlowKeys.Initial, { screen: InitFlow.LogIn })
  }

  render() {
    const { t } = this.props;
    const { showRemoveButton, showBiometricAuth, createPin } = this.state;

    return (
      <Container>
        {
          createPin ? (
            <Header
              hideBackArrow
              hideLogo
            />
          ) : (
              <View style={styles.header}>
                <View style={styles.userBlock}>
                  <Image
                    source={USER_DEFAULT_IMAGE}
                    style={styles.userImage}
                    resizeMode="contain"
                  />
                  <View style={styles.userData}>
                    <Text style={styles.name}>Александр Иванов</Text>
                    <Text style={styles.email}>alex@kukutro.com</Text>
                  </View>
                </View>
                <ButtonLink
                  title={t('common:buttons.it_is_not_me')}
                  onPress={this.logOut}
                  contentContainerStyle={styles.headerLink}
                />
              </View>
            )
        }

        <View style={styles.container}>
          {
            createPin && (
              <View style={styles.topContent}>
                <Text style={styles.title}>{t(`auth_flow:set_pincode`)}</Text>
              </View>
            )
          }
          <PinView
            ref={this.pinView}
            pinLength={4}
            buttonSize={scale(75)}
            onValueChange={this.setEnteredPin}
            buttonTextStyle={styles.buttonTextStyle}
            buttonViewStyle={styles.buttonViewStyle}
            inputViewEmptyStyle={styles.inputViewEmptyStyle}
            inputViewFilledStyle={styles.inputViewFilledStyle}
            buttonDescrStyle={styles.buttonDescrStyle}
            inputAreaStyle={styles.inputAreaStyle}
            onButtonPress={key => {
              if (key === "custom_left") {
                alert("Biometric")
              }
              if (key === "custom_right") {
                this.pinView.current.clear()
              }
            }}
            customLeftButton={showBiometricAuth ? <Image source={FACE_SMALL_BLACK_ICON} style={styles.biometricIcon} /> : undefined}
            customRightButton={showRemoveButton ? <Image source={CLOSE_SMALL_BLACK_ICON} style={styles.deleteIcon} /> : undefined}
          />
          <View style={styles.bottomBlock}>
            <ButtonLink
              title={t('common:buttons.forget_pin_code')}
              onPress={() => { }}
            />
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  is_authorized: state.authReducer.is_authorized
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIsAuthorized
    },
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SecurityScreen),
);


SecurityScreen.propTypes = {
  createPin: PropTypes.bool,
  changePin: PropTypes.bool,
};