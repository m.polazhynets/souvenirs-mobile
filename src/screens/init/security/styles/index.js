import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1, COLOR_GRAY_4, COLOR_GRAY_5, COLOR_BLUE, COLOR_BLACK } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, FONT_REGULAR_DISPLAY, scale, v_scale, FONT_BOLD } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall, smaller, megaLarge, regular } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: v_scale(36),
    paddingHorizontal: scale(20)
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    textAlign: 'center',
    color: COLOR_GRAY_1,
    letterSpacing:  -0.08,
  },
  inputViewEmptyStyle: {
    backgroundColor: COLOR_GRAY_4,
    width: scale(12),
    height: scale(12),
    margin: scale(12)
  },
  inputViewFilledStyle: {
    backgroundColor: COLOR_BLUE,
    width: scale(12),
    height: scale(12),
    margin: scale(12)
  },
  buttonTextStyle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR_DISPLAY,
    fontSize: megaLarge,
  },
  buttonViewStyle: {
    backgroundColor: COLOR_GRAY_5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonDescrStyle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    fontSize: smaller,
    letterSpacing: 3,
    marginTop: -v_scale(5),
    textAlign: 'center',
  },
  inputAreaStyle: {
    marginVertical: v_scale(50)
  },
  deleteIcon: {
    width: scale(11),
    height: scale(11)
  },
  biometricIcon: {
    width: scale(25),
    height: scale(25)
  },
  bottomBlock: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: v_scale(20)
  },
  header: {
    flexDirection: 'row',
    padding: scale(16),
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  userImage: {
    width: scale(40),
    height: scale(40),
    marginRight: scale(12)
  },
  userBlock: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    fontFamily: FONT_BOLD,
    color: COLOR_BLACK,
    fontSize: regular,
    letterSpacing: -0.24
  },
  email: {
    fontFamily: FONT_REGULAR,
    color: COLOR_BLACK,
    fontSize: regularSmall,
    letterSpacing: -0.08,
    opacity: 0.48
  },
  headerLink: {
    width: 'auto'
  }
})