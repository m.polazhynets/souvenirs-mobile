import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1 } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1
  },

  buttonWrapper: {
    marginTop: v_scale(6)
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: v_scale(36),
    paddingHorizontal: scale(20)
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  description: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    textAlign: 'center',
    color: COLOR_GRAY_1,
    letterSpacing:  -0.08,
  },
 
})