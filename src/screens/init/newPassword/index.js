import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Button, Input, Container, Header } from '../../../components';
import styles from './styles';

class NewPasswordScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header />
        <KeyboardAwareScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:new_password')}</Text>
            <Text style={styles.description}>{t('auth_flow:password_description')}</Text>
          </View>
          <Input
            placeholder={t('common:form:placeholders.create_password')}
            value={null}
            label={t('common:form:labels.password')}
            required
            secureTextEntry
            isValid={null}
            onBlur={() => { }}
            onChangeText={(val) => { }}
          />
          <Input
            placeholder={t('common:form:placeholders.repeat_password')}
            value={null}
            label={t('common:form:labels.repeat')}
            required
            secureTextEntry
            isValid={null}
            onBlur={() => { }}
            onChangeText={(val) => { }}
          />
            <Button
              title={t('common:buttons.set_new_password')}
              contentContainerStyle={styles.buttonWrapper}
              onPress={() => { }}
              type="primary"
              isLoading={false}
            />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(NewPasswordScreen),
);