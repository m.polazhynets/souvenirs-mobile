import React, { Component } from 'react';
import { Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { replace } from '../../../navigator/RootNavigation';
import { InitFlow } from '../../../navigator/Keys';
import { Header, Container, GroupButtons } from '../../../components';
import PrivatForm from './components/privatForm';
import OrganizationForm from './components/organizationForm';
import styles from './styles';

class SignUpPrivateDataScreen extends Component {
  state = {
    activeTab: 0 
  }

  onChangeTab = (key) => {
    this.setState({activeTab : key});
  }

  render() {
    const { t } = this.props;
    const { activeTab } = this.state;
    const GROUP_BUTTONS = [{
      key: 0,
      title: t('auth_flow:i_am_private_person')
    },{
      key: 1,
      title: t('auth_flow:we_are_organization')
    }]

    return (
      <Container>
        <Header />
        <KeyboardAwareScrollView  style={styles.flex} contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
          <Text style={styles.title}>{t('auth_flow:private_data')}</Text>
          <GroupButtons
            data={GROUP_BUTTONS}
            active={activeTab}
            onPress={this.onChangeTab}
            stylesContainer={styles.groupButton}
          />
          {activeTab === 0 ? (
            <PrivatForm
              is_request={false}
              onSubmit={(data) => replace(InitFlow.Security, { createPin: true })}
            />
          ): (
            <OrganizationForm
              is_request={false}
              onSubmit={(data) => replace(InitFlow.Security)}
            />
          )}

        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SignUpPrivateDataScreen),
);