import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import _ from 'underscore';
import { withTranslation } from 'react-i18next';
import { Button, Input, PhotoUpload } from '../../../../components';
import { USER_DEFAULT_IMAGE, PLUS_ICON } from '../../../../constants/Images';
import { validateEmail, validatePassword, validateText } from '../../../../utils/Helper'
import styles from '../styles';

class PrivatFormComponent extends Component {
  state = {
    form: {
      phone: {
        value: '',
        isValid: null,
      },
      email: {
        value: '',
        isValid: null,
      },
      first_name: {
        value: '',
        isValid: null,
      },
      password: {
        value: '',
        isValid: null,
      },
      invite: {
        value: '',
        isValid: null,
      },
      avatar: {
        value: null,
        isValid: null,
      }
    }
  }

  validate = (val, name) => {
    const { form } = this.state;

    switch (name) {
      case 'password':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validatePassword(val)
            },
          }
        })
        break
      case 'phone':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: (val.length === 18) ? true : false
            },
          }
        })
        break
      case 'first_name':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateText(val, 3)
            },
          }
        })
        break
      case 'invite':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: val.length > 3 ? true : false
            },
          }
        })
        break
      case 'avatar':
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: val ? true : false
            },
          }
        })
        break
      default:
        this.setState({
          form: {
            ...form,
            [name]: {
              ...form[name],
              value: val,
              isValid: validateEmail(val)
            }
          }
        })
        break
    }
  }

  checkFullValidate = async () => {
    const { form: { avatar, phone, email, first_name, password } } = this.state;

    return new Promise((resolve, reject) => {
      this.setState({
        form: {
          ...this.state.form,
          phone: {
            ...phone,
            isValid: (phone.isValid === true)
          },
          avatar: {
            ...avatar,
            isValid: (avatar.isValid === true)
          },
          email: {
            ...email,
            isValid: (email.isValid === true)
          },
          first_name: {
            ...first_name,
            isValid: (first_name.isValid === true)
          },
          password: {
            ...password,
            isValid: (password.isValid === true)
          },
        }
      }, () => {
        const { form: { avatar, phone, email, first_name, password } } = this.state;

        (email.isValid && password.isValid && avatar.isValid && phone.isValid && first_name.isValid) ? resolve(true) : reject(false)
      })
    });
  }

  handleSubmit = async () => {
    const { onSubmit } = this.props;
    const { form: { avatar, phone, email, first_name, password, invite } } = this.state;

    try {
      //await this.checkFullValidate();

      onSubmit({
        avatar: avatar.value,
        email: email.value,
        password: password.value,
        phone: phone.value,
        first_name: first_name.value,
        invite: invite.value
      })
    } catch (err) {
      console.log('error validation')
    }
  };

  render() {
    const { is_request, t } = this.props;
    const { form: { avatar, phone, email, first_name, password, invite } } = this.state;
  
    return (
      <View>
        <PhotoUpload
          onPhotoSelect={avatar => {
            if (avatar) {
              this.validate(avatar, 'avatar')
            }
          }}
          imagePickerProps={{
            title: t('common:buttons.choose_photo'),
            cancelButtonTitle: t('common:buttons.cancel'),
            takePhotoButtonTitle: t('common:buttons.create_photo'),
            chooseFromLibraryButtonTitle: t('common:buttons.choose_from_gallery'),
          }}
          onError={(err) => {
            Toast.show(err);
          }}
          containerStyle={styles.uploaderWrapper}
        >
          <View style={styles.imageWrapper}>
            <Image
              style={[styles.image, (avatar.isValid === false) && styles.image_invalid]}
              resizeMode='cover'
              source={(avatar.value) ? {uri: `data:image/jpeg;base64,${avatar.value}`} : USER_DEFAULT_IMAGE}
            />
            {
              !avatar.isValid && (
                <View style={styles.uploader}>
                  <Image
                    style={styles.plus_logo}
                    resizeMode='cover'
                    source={PLUS_ICON}
                  />
                  <Text style={styles.uploaderText}>{t('auth_flow:avatar')}</Text>
                </View>
              )
            }
          </View>
        </PhotoUpload>
        <Input
          mask={"+7 (999) 999 99 99"}
          label={t('common:form:labels.phone_number')}
          maxLength={18}
          keyboardType={'numeric'}
          placeholder={t('common:form:placeholders.write_phone_number')}
          value={phone.value}
          required
          isValid={phone.isValid}
          onBlur={() => this.validate(phone.value, 'phone')}
          onChangeText={(val) => this.validate(val, 'phone')}
        />
        <Input
          label={t('common:form:labels.email')}
          placeholder={t('common:form:placeholders.write_email')}
          value={email.value}
          required
          isValid={email.isValid}
          onBlur={() => this.validate(email.value, 'email')}
          onChangeText={(val) => this.validate(val, 'email')}
        />
        <Input
          label={t('common:form:labels.your_name')}
          placeholder={t('common:form:placeholders.write_first_name')}
          value={first_name.value}
          required
          isValid={first_name.isValid}
          onBlur={() => this.validate(first_name.value, 'first_name')}
          onChangeText={(val) => this.validate(val, 'first_name')}
        />
        <Input
          label={t('common:form:labels.password')}
          placeholder={t('common:form:placeholders.write_password')}
          value={password.value}
          required
          isValid={password.isValid}
          secureTextEntry
          onBlur={() => this.validate(password.value, 'password')}
          onChangeText={(val) => this.validate(val, 'password')}
          noMargin
        />
        <Input
          containerStyles={styles.inputMarginTop}
          label={t('common:form:labels.invite')}
          placeholder={t('common:form:placeholders.write_referal_code')}
          value={invite.value}
          onBlur={() => this.validate(invite.value, 'invite')}
          onChangeText={(val) => this.validate(val, 'invite')}
          noMargin
        />
        <View style={styles.textsBlock}>
          <Text style={styles.description}>{t('auth_flow:invite_description')}</Text>
          <View style={styles.linkBlock}>
            <Text style={styles.description}>{t('auth_flow:i_agree_with')}</Text>
            <TouchableOpacity
              onPress={() => { }}
              activeOpacity={0.8}
            >
              <Text style={styles.description, styles.link}>{` ${t('auth_flow:terms_of_use')} `}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Button
          title={t('common:buttons.sign_up')}
          contentContainerStyle={styles.buttonWrapper}
          onPress={this.handleSubmit}
          type="primary"
          isLoading={is_request}
        />
      </View>
    );
  }
}

export default withTranslation(['common', 'auth_flow'])(PrivatFormComponent);


Input.PrivatFormComponent = {
  is_request: PropTypes.bool,
  onSubmit: PropTypes.func,
};