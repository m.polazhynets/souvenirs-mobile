import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1, COLOR_BLUE, COLOR_WHITE, COLOR_RED } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall, small } = variables.fontSize;

export default StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    padding: scale(16),
  },
  buttonWrapper: {
    marginTop: v_scale(6)
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: v_scale(36),
    paddingHorizontal: scale(20)
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  description: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    textAlign: 'center',
    color: COLOR_GRAY_1,
    letterSpacing:  -0.08,
    
  },
  groupButton: {
    marginTop: scale(25),
    marginBottom: scale(32)
  },
  image: {
    width: scale(110),
    height: scale(110),
    backgroundColor: COLOR_WHITE,
    overflow: 'hidden',
    borderRadius: scale(55),
  },
  image_invalid: {
    borderWidth: 1,
    borderColor: COLOR_RED,
  },
  imageWrapper: {
    position: 'relative'
  },
  plus_logo: {
    width: scale(22),
    height: scale(22),
  },
  uploaderText: {
    fontFamily: FONT_REGULAR,
    fontSize: small,
    textAlign: 'center',
    color: COLOR_WHITE,
    letterSpacing:  -0.08,
  },
  uploader: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1
  },
  uploaderWrapper: {
    marginBottom: v_scale(20)
  },
  inputMarginTop: {
    marginTop: v_scale(32)
  },
  textsBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: v_scale(16),
    paddingBottom: v_scale(24),
    paddingHorizontal: scale(30)
  },
  link: {
    color: COLOR_BLUE,
  },
  linkBlock: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    marginTop: v_scale(50)
  }
})