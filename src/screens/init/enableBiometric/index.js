import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Button, Container, Header, ButtonLink } from '../../../components';
import { navigate } from '../../../navigator/RootNavigation';
import { InitFlow } from '../../../navigator/Keys';
import { FACE_BLUE_ICON } from '../../../constants/Images';
import styles from './styles';


class EnableBiometricScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header
          hideBackArrow
          hideLogo
          rightBtn={(
            <View style={styles.headerButtonWrapper}>
              <ButtonLink
                title={t('common:buttons.skip')}
                onPress={() => navigate(InitFlow.InviteFriends)}
                contentContainerStyle={styles.headerButton}
              />
            </View>
          )}
        />
        <View style={styles.container}>
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:auto_use_face_id')}</Text>
          </View>
          <View style={styles.centerContent}>
            <Image source={FACE_BLUE_ICON} style={styles.image} />
            <Text style={styles.text}>{t('auth_flow:biometric_description')}</Text>
          </View>
          <View style={styles.bottomContent}>
            <View style={styles.emptyBlock}/>
            <Button
              title={t('common:buttons.enable_login_using_face_id')}
              onPress={() => navigate(InitFlow.InviteFriends)}
              type="primary"
              isLoading={false}
            />
          </View>
          
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(EnableBiometricScreen),
);