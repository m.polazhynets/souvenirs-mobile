import { StyleSheet } from 'react-native';
import { COLOR_GRAY_1, COLOR_BLACK } from '../../../../constants/Colors';
import variables, { FONT_BOLD_DISPLAY, FONT_REGULAR, scale, v_scale } from '../../../../constants/StylesConstants';

const { extLarge, regularSmall, mainRegular } = variables.fontSize;

export default StyleSheet.create({
  container: {
    padding: scale(16),
    flexGrow: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  topContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(20),
  },
  title: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: extLarge,
    textAlign: 'center',
    letterSpacing: 0.337647,
    paddingBottom: v_scale(10)
  },
  image: {
    width: scale(50),
    height: scale(50),
    marginBottom: v_scale(25)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    textAlign: 'center',
    color: COLOR_BLACK,
    letterSpacing:  -0.41,
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
  },
  headerButtonWrapper: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  headerButton: {
    width: 'auto'
  },
  emptyBlock: {
    height: v_scale(95)
  },
  bottomContent: {
    width: '100%'
  }
})