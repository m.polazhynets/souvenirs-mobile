import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header, Button } from '../../../components';
import { COPY_ICON } from '../../../constants/Images';
import styles from './styles';


class OrderToOfficeScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:order_products_to_office')}</Text>
        <ScrollView style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.text}>{t('profile_flow:order_to_office_description')}</Text>
          </View>
          <View style={styles.copyBlock}>
              <Button
                title={'+7 (495) 346-4433'}
                onPress={() => {}}
                type="primary"
                contentContainerStyle={styles.flex}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {}}
                style={styles.copyButton}
              >
                <Image
                  source={COPY_ICON}
                  resizeMode="contain"
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.copyBlock}>
              <Button
                title={t('common:buttons.write_email')}
                onPress={() => {}}
                type="primary"
                contentContainerStyle={styles.flex}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {}}
                style={styles.copyButton}
              >
                <Image
                  source={COPY_ICON}
                  resizeMode="contain"
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(OrderToOfficeScreen),
);