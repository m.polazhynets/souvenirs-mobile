import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_REGULAR_DISPLAY } from '../../../../constants/StylesConstants';

const { large } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0,
  },
  content: {
    paddingTop: v_scale(9),
    paddingVertical: v_scale(12)
  },
  text: {
    fontFamily: FONT_REGULAR_DISPLAY,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK
  },
  icon: {
    width: scale(24),
    height: scale(24)
  },
  copyBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: v_scale(10)
  },
  flex: {
    flex: 1
  },
  copyButton: {
    paddingLeft: scale(16),
    paddingVertical: scale(10),
    marginLeft: scale(8),
  }
})