import { StyleSheet } from 'react-native';
import { COLOR_GRAY_6, COLOR_BLACK_1, COLOR_BLACK, COLOR_BLUE } from '../../../../constants/Colors';
import variables, { scale, FONT_REGULAR, v_scale, FONT_BOLD, FONT_BOLD_DISPLAY, deviceHeight } from "../../../../constants/StylesConstants";

const { biggerlarge, mainRegular, smaller, regularSmall } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title
  },

  container: {
    padding: scale(16),
  },
  bottomContainer: {
    paddingRight: 0,
    marginBottom: v_scale(20)
  },  
  avatar: {
    width: scale(87),
    height: scale(87)
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerContent: {
    justifyContent: 'space-between',
  },
  name: {
    ...variables.title,
    fontSize: biggerlarge
  },
  email: {
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    color: COLOR_GRAY_6,
    letterSpacing: -0.41
  },
  headerLink: {
    width: 'auto', 
    alignItems: 'flex-start'
  },
  levelImage: {
    width: scale(100),
    height: scale(100)
  },
  levelBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: v_scale(36),
    width: '100%'
  },
  levelContent: {
    justifyContent: 'space-between',
    flex: 1,
    paddingRight: scale(30)
  },
  levelText: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    letterSpacing: -0.08,
    color: COLOR_BLACK_1,
  },
  gray: {
    color: COLOR_GRAY_6
  },
  secondGray: {
    color: '#E0E0E0',
    marginTop: v_scale(5)
  },
  levelTitle: {
    fontSize: biggerlarge,
    fontFamily: FONT_BOLD,
  },
  amountBlock: {
    marginTop: v_scale(40),
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  amountFirst: {
    flex: 4
  },
  amountSecond: {
    flex: 2
  },
  amount: {
    ...variables.title
  },
  amountSmall: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: biggerlarge,
    color: '#4F4F4F',
    letterSpacing: 0.41,
  },
  thirdGray: {
    color: '#4F4F4F',
  },
  amountText: {
    fontFamily: FONT_REGULAR,
    fontSize: regularSmall,
    letterSpacing: -0.08,
    color: COLOR_BLACK,
  },
  blue: {
    color: COLOR_BLUE
  },
  buttonBlock: {
    padding: scale(16),
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    alignItems: 'center'
  },
  description: {
    fontFamily: FONT_REGULAR,
    fontSize: smaller,
    letterSpacing: -0.08,
    color: COLOR_GRAY_6,
    textAlign: 'center',
    marginTop: v_scale(5),
    width: '60%'
  }
})