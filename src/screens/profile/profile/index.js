import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import i18n from '../../../utils/i18n';
import { setIsAuthorized } from '../../../actions/authActions';
import { Container, ListItem, ButtonLink } from '../../../components';
import { replace, navigate } from '../../../navigator/RootNavigation';
import { USER_DEFAULT_IMAGE, LEVEL_LION_IMAGE } from '../../../constants/Images';
import { InitFlow, FlowKeys, MenuFlow, ProfileFlow } from '../../../navigator/Keys';
import styles from './styles';

const MENU_LIST = [
  {
    key: '1',
    title: i18n.t('profile_flow:arrival_history'),
    comments: '6',
    navigate: ProfileFlow.ArrivedHistory
  }, {
    key: '2',
    title: i18n.t('profile_flow:transaction_history'),
    navigate: ProfileFlow.Transaction
  }, {
    key: '3',
    title: i18n.t('profile_flow:order_products_to_office'),
    navigate: ProfileFlow.OrderToOffice
  }, {
    key: '4',
    title: i18n.t('profile_flow:warn_about_arrival'),
  }, {
    key: '5',
    title: i18n.t('profile_flow:statistics_number_tourists'),
    navigate: ProfileFlow.Statistics
  }
]

class ProfileScreen extends Component {

  onNavigate = (item) => {
    if (!item.navigate) return;

    navigate(item.navigate)
  }

  render() {
    const { t } = this.props;

    return (
      <Container>
        <View style={styles.content}>
          <ScrollView >
            <View style={styles.container}>
              <View style={styles.header}>
                <View style={styles.headerContent}>
                  <ButtonLink
                    contentContainerStyle={styles.headerLink}
                    onPress={() => navigate(ProfileFlow.Settings)}
                    title={t('profile_flow:settings')}
                  />
                  <View>
                    <Text style={styles.name}>Александр Иванов</Text>
                    <Text style={styles.email}>ivanov@gmail.com</Text>
                  </View>
                </View>
                <Image
                  source={USER_DEFAULT_IMAGE}
                  style={styles.avatar}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.levelBlock}>
                <View style={styles.levelContent}>
                  <Text style={[styles.levelText, styles.gray]}>{t('profile_flow:your_level_program').toUpperCase()}</Text>
                  <View>
                    <Text style={[styles.levelText, styles.levelTitle]}>ЛЕВ</Text>
                    <Text style={styles.levelText}>30% от суммы с каждой покупки зачисляется в виде бонусов</Text>
                    <Text style={[styles.levelText, styles.secondGray]}>Далее — Гриффон</Text>
                  </View>
                </View>
                <Image
                  source={LEVEL_LION_IMAGE}
                  style={styles.levelImage}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.amountBlock}>
                <View style={styles.amountFirst}>
                  <Text style={styles.amount}>65 401</Text>
                  <Text style={styles.amountText}>{t('profile_flow:points_accumulated')}</Text>
                  <TouchableOpacity><Text style={[styles.amountText, styles.blue]}>{t('profile_flow:how_to_spend')}?</Text></TouchableOpacity>
                </View>
                <View style={styles.amountSecond}>
                  <Text style={styles.amountSmall}>65</Text>
                  <Text style={[styles.amountText, styles.thirdGray]}>{t('profile_flow:friends_invited')}</Text>
                </View>
                <View style={styles.amountSecond}>
                  <Text style={styles.amountSmall}>401</Text>
                  <Text style={[styles.amountText, styles.thirdGray]}>{t('profile_flow:points_from_friends')}</Text>
                </View>
              </View>
            </View>
            <View style={styles.buttonBlock}>
              <Button
                title={t('common:buttons.invite_friends')}
                onPress={() => navigate(ProfileFlow.InviteFriendsProfile)}
                type="primary"
              />
              <Text style={styles.description}>{t('profile_flow:invite_description')}</Text>
            </View>
            <View style={[styles.container, styles.bottomContainer]}>
              {MENU_LIST.map(item => <ListItem {...item} onPress={() => this.onNavigate(item)} />)}
            </View>
          </ScrollView>
        </View>

      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIsAuthorized
    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(ProfileScreen),
);