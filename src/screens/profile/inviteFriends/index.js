import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Clipboard } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Button, Container, Header } from '../../../components';
import { MULTIPLE_USERS_ICON, COPY_ICON } from '../../../constants/Images';
import styles from './styles';

class InviteFriendScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header hideLogo />
        <View style={styles.container}>
          <View style={styles.topContent}>
            <Text style={styles.title}>{t('auth_flow:invite_friends')}</Text>
          </View>
          <View style={styles.centerContent}>
            <Image source={MULTIPLE_USERS_ICON} style={styles.image} />
            <Text style={styles.text}>{t('auth_flow:invite_description_1')}</Text>
          </View>
          <View style={styles.bottomContent}>
            <TouchableOpacity 
              style={styles.copyBlock}
              activeOpacity={0.5}
              onPress={() => Clipboard.setString('QJKFJALDJFS')}
            >
              <Text style={styles.copyText}>QJKFJALDJFS</Text>
              <Image source={COPY_ICON} style={styles.copyIcon} />
            </TouchableOpacity>
            <Button
              title={t('common:buttons.share_invitation_code')}
              onPress={() => {}}
              type="primary"
              isLoading={false}
            />
          </View>
          
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch,
  );

export default withTranslation(['common', 'auth_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(InviteFriendScreen),
);