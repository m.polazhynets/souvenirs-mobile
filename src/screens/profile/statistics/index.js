import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header } from '../../../components';
import styles from './styles';

const DEMO = [{
  key: '0',
  title: '23 Февраля',
  descr: 'Покупка в магазине',
  value: '+ 1246.00',
  comment: '23 туриста'
},{
  key: '1',
  title: '23 Февраля',
  descr: 'Покупка в магазине',
  value: '+ 1105.00',
  comment: '23 туриста'
}]

class StatisticsScreen extends Component {

  renderItem = ({title, value, descr, key, comment}) => {
    return (
      <View style={styles.item} key={key}>
        <View style={styles.leftSide}>
          <Text style={styles.itemTitle}>{title}</Text>
          <Text style={styles.itemText}>{descr}</Text>
        </View>
        <View>
          <Text style={[styles.itemTitle, styles.success]}>{value}</Text> 
          <Text style={styles.itemText}>{comment}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:statistics')}</Text>
        <ScrollView>
          <View style={styles.content}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>1 Января — 3 Марта</Text>
            </View>
            <View style={styles.block}>
              <View style={styles.blockTitle}>
                <Text style={styles.titleStyle}>Всего</Text>
                <Text style={styles.titleStyle}>2340 туристов</Text>
              </View>
            </View>
            <View style={styles.block}>
              <View style={styles.blockTitle}>
                <Text style={styles.titleStyle}>Февраль 2020</Text>
                <Text style={styles.titleStyle}>40 туристов</Text>
              </View>
              {DEMO.map(item => this.renderItem(item))}
            </View>
            <View style={styles.block}>
              <View style={styles.blockTitle}>
                <Text style={styles.titleStyle}>Февраль 2020</Text>
                <Text style={styles.titleStyle}>40 туристов</Text>
              </View>
              {DEMO.map(item => this.renderItem(item))}
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(StatisticsScreen),
);