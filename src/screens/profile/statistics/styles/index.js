import { StyleSheet } from 'react-native';
import { COLOR_BLACK, COLOR_GREEN, COLOR_BLUE, COLOR_GRAY_5 } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_BOLD_DISPLAY, FONT_MEDIUM } from '../../../../constants/StylesConstants';

const { large, mainRegular, regular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },

  content: {
    paddingTop: v_scale(4),
  },
  itemText: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    letterSpacing: -0.24,
    color: COLOR_BLACK,
    opacity: 0.48
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: v_scale(16),
    paddingLeft: 0,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    marginLeft: scale(16)
  },
  itemTitle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    letterSpacing: -0.41
  },
  success: {
    color: COLOR_GREEN
  },
  blockTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(16)
  },  
  titleStyle: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: large,
    color: COLOR_BLACK, 
    letterSpacing: 0.35,
    marginBottom: v_scale(10)
  },
  block: {
    paddingTop: v_scale(30)
  },

  button: {
    backgroundColor: COLOR_GRAY_5,
    borderRadius: scale(10),
    paddingVertical: v_scale(15),
    marginHorizontal: scale(16),
  },
  buttonText: {
    color: COLOR_BLUE,
    fontFamily: FONT_MEDIUM,
    fontSize: mainRegular,
    letterSpacing: -0.41,
    textAlign: 'center'
  }
})