import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR } from '../../../../constants/StylesConstants';

const { large } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    marginBottom: v_scale(16),
    paddingHorizontal: scale(16)
  },
  topContent: {
    padding: scale(16),
    paddingTop: v_scale(4),
    paddingBottom: 0
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK
  },
  paddingLeft: {
    paddingLeft: scale(16)
  },
  marginBottom: {
    marginBottom: v_scale(30)
  },
  bottomContent: {
    padding: scale(16),
    paddingTop: v_scale(30),
    paddingRight: 0
  },

})