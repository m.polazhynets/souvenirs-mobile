import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import i18n from '../../../utils/i18n';
import { Container, Header, SwitchItem, ListItem } from '../../../components';
import { navigate } from '../../../navigator/RootNavigation';
import { ProfileFlow } from '../../../navigator/Keys';
import styles from './styles';

const LIST = [{
  key: 0,
  title: i18n.t('profile_flow:earning_writing_off_points'),
}, {
  key: 1,
  title: i18n.t('profile_flow:notification'),
  navigate: ProfileFlow.NotifySettings
}]

class SettingsScreen extends Component {
  state = {
    pin_input: true,
    face_input: true
  }

  onChangeSwitch = ({ value, name }) => {
    this.setState({ [name]: value });
  }

  onNavigate = (item) => {
    if (!item.navigate) return;

    navigate(item.navigate)
  }

  render() {
    const { t } = this.props;
    const { pin_input, face_input } = this.state;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:settings')}</Text>
        <ScrollView style={styles.container}>
          <View style={styles.topContent}>
            <SwitchItem
              onPress={this.onChangeSwitch}
              title={t('profile_flow:pin_input')}
              value={pin_input}
              name={'pin_input'}
            />
            <SwitchItem
              onPress={this.onChangeSwitch}
              title={t('profile_flow:face_input')}
              value={face_input}
              name={'face_input'}
            />
          </View>
          <ListItem
            onPress={() => { }}
            title={t('profile_flow:change_pin')}
            contentContainerStyle={styles.paddingLeft}
          />
          <View style={styles.bottomContent}>
            <ListItem
              onPress={() => { }}
              title={t('profile_flow:languare')}
              comments={t('profile_flow:system_language')}
              contentContainerStyle={styles.marginBottom}
            />
            {LIST.map(item => (
              <ListItem
                key={item.key}
                onPress={() => this.onNavigate(item)}
                title={item.title}
              />
            ))}
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SettingsScreen),
);