import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header } from '../../../components';
import styles from './styles';

const DEMO = [{
  key: '0',
  title: 'Сегодня',
  value: '16:00'
},{
  key: '1',
  title: 'Вчера',
  value: '16:00'
},{
  key: '2',
  title: 'Сегодня',
  value: '16:00'
}]

class ArrivedHistoryScreen extends Component {

  renderItem = ({title, value, key}) => {
    return (
      <View style={styles.item} key={key}>
        <Text style={styles.itemTitle}>{title}</Text> 
        <Text style={[styles.itemTitle, styles.itemValue]}>{value}</Text> 
      </View>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:arrival_history')}</Text>
        <ScrollView style={styles.container}>
          
          <View style={styles.content}>
            <View style={styles.block}>
              <Text style={styles.blockTitle}>Февраль 2020</Text>
              {DEMO.map(item => this.renderItem(item))}
            </View>
            <View style={styles.block}>
              <Text style={styles.blockTitle}>Февраль 2020</Text>
              {DEMO.map(item => this.renderItem(item))}
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(ArrivedHistoryScreen),
);