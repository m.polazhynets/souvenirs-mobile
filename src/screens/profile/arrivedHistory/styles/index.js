import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_BOLD_DISPLAY } from '../../../../constants/StylesConstants';

const { large, mainRegular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0
  },
  content: {
    paddingTop: v_scale(4),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: v_scale(18)
  },
  itemTitle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    letterSpacing: -0.41
  },
  itemValue: {
    opacity: 0.4
  },
  blockTitle: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: large,
    color: COLOR_BLACK, 
    letterSpacing: 0.35,
    marginBottom: v_scale(10)
  },
  block: {
    paddingTop: v_scale(15)
  }
})