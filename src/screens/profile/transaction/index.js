import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header } from '../../../components';
import styles from './styles';

const DEMO = [{
  key: '0',
  title: 'Начисление бонусов',
  descr: 'Покупка в магазине',
  value: '+ 1246.00'
},{
  key: '1',
  title: 'Бонус от пользователя',
  descr: 'Илья Омельянчук',
  value: '+ 1105.00'
}]

class TransactionScreen extends Component {

  renderItem = ({title, value, descr, key}) => {
    return (
      <View style={styles.item} key={key}>
        <View style={styles.leftSide}>
          <Text style={styles.itemTitle}>{title}</Text>
          <Text style={styles.itemText}>{descr}</Text>
        </View>
        <Text style={[styles.itemTitle, styles.success]}>{value}</Text> 
      </View>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:transaction')}</Text>
        <ScrollView style={styles.container}>
          <View style={styles.content}>
            <View style={styles.block}>
              <Text style={styles.blockTitle}>Февраль 2020</Text>
              {DEMO.map(item => this.renderItem(item))}
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(TransactionScreen),
);