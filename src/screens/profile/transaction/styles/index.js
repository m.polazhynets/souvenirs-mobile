import { StyleSheet } from 'react-native';
import { COLOR_BLACK, COLOR_GREEN } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_BOLD_DISPLAY } from '../../../../constants/StylesConstants';

const { large, mainRegular, regular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0,
    paddingRight: 0
  },
  content: {
    paddingTop: v_scale(4),
  },
  itemText: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    letterSpacing: -0.24,
    color: COLOR_BLACK,
    opacity: 0.48
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: v_scale(16),
    paddingLeft: 0,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)'
  },
  itemTitle: {
    color: COLOR_BLACK,
    fontFamily: FONT_REGULAR,
    fontSize: mainRegular,
    letterSpacing: -0.41
  },
  success: {
    color: COLOR_GREEN
  },
  blockTitle: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: large,
    color: COLOR_BLACK, 
    letterSpacing: 0.35,
    marginBottom: v_scale(10)
  },
  block: {
    paddingTop: v_scale(15)
  }
})