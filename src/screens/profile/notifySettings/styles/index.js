import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale } from '../../../../constants/StylesConstants';

const { large } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0
  },
  content: { 
    paddingTop: v_scale(4),
  },
})