import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import i18n from '../../../utils/i18n';
import { Container, Header, SwitchItem } from '../../../components';
import styles from './styles';

const LIST = [{
  key: 0,
  title: i18n.t('profile_flow:about_bonus_points'),
  name: 'bonus_points'
}, {
  key: 1,
  title: i18n.t('profile_flow:about_registering_invitees_users'),
  name: 'invitees_users'
},{
  key: 2,
  title: i18n.t('profile_flow:sales'),
  name: 'sales'
},{
  key: 3,
  title: i18n.t('profile_flow:hot_offers'),
  name: 'hot_offers'
},{
  key: 4,
  title: i18n.t('profile_flow:shop_news'),
  name: 'shop_news'
},{
  key: 5,
  title: i18n.t('profile_flow:shop_events'),
  name: 'shop_events'
}]

class NotificationSettingsScreen extends Component {
  state = {
  }

  onChangeSwitch = ({ value, name }) => {
    this.setState({ [name]: value });
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo />
        <Text style={styles.title}>{t('profile_flow:notification')}</Text>
        <ScrollView style={styles.container}>
          <View style={styles.content}>
            {LIST.map(item => (
              <SwitchItem
                onPress={this.onChangeSwitch}
                title={item.title}
                value={this.state[item.name]}
                name={item.name}
              />
            ))}
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'profile_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(NotificationSettingsScreen),
);