import { StyleSheet } from 'react-native';
import { COLOR_BLACK  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, deviceWidth } from '../../../../constants/StylesConstants';

const { regular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
    paddingBottom: v_scale(16)
  },
  container: {
    padding: scale(16),
    paddingTop: 0,
  },
  content: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    paddingTop: v_scale(34),
    paddingBottom: v_scale(50)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    letterSpacing: -0.08,
    color: COLOR_BLACK,
    textAlign: 'center',
    width: '70%'
  },
  code: {
    width: scale(204),
    height: scale(204)
  },
  corner: {
    width: scale(38),
    height: scale(38),
    position: 'absolute'
  },
  topLeft: {
    top: 0,
    left: 0
  },
  topRight: {
    top: 0,
    right: 0,
    transform: [{ rotate: '90deg'}]
  },
  bottomLeft: {
    bottom: 0,
    left: 0,
    transform: [{ rotate: '270deg'}]
  },
  bottomRight: {
    bottom: 0,
    right: 0,
    transform: [{ rotate: '180deg'}]
  },
  codeWrapper: {
    position: 'relative',
    width: deviceWidth*0.6,
    height: deviceWidth*0.6,
    justifyContent: 'center',
    alignItems: 'center'
  }
})