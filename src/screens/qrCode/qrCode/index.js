import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header, ButtonLink } from '../../../components';
import { navigate } from '../../../navigator/RootNavigation';
import { QRCodeFlow } from '../../../navigator/Keys';
import { QR_CODE_IMAGE, CORNER_IMAGE } from '../../../constants/Images';
import styles from './styles';



class QRCodeScreen extends Component {

  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header hideLogo hideBackArrow/>
        <Text style={styles.title}>{t('code_flow:qr_code')}</Text>
        <ScrollView style={styles.container} contentContainerStyle={{flex: 1}}>
          <View style={styles.content}>
            <Text style={styles.text}>{t('code_flow:qr_code_description')}</Text>
            <View style={styles.codeWrapper}>
              <Image
                source={CORNER_IMAGE}
                resizeMode="contain"
                style={[styles.corner, styles.topLeft]}
              />
              <Image
                source={CORNER_IMAGE}
                resizeMode="contain"
                style={[styles.corner, styles.topRight]}
              />
              <Image
                source={CORNER_IMAGE}
                resizeMode="contain"
                style={[styles.corner, styles.bottomLeft]}
              />
              <Image
                source={CORNER_IMAGE}
                resizeMode="contain"
                style={[styles.corner, styles.bottomRight]}
              />
              <Image
                source={QR_CODE_IMAGE}
                style={styles.code}
                resizeMode="contain"
              />
            </View>
            <ButtonLink
              title={t('common:buttons.how_it_works')}
              onPress={() => navigate(QRCodeFlow.HowItWorks)}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'code_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(QRCodeScreen),
);