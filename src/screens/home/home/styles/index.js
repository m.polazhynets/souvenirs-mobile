import { StyleSheet, Platform } from 'react-native';
import { COLOR_BLACK, COLOR_BLUE  } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_BOLD, FONT_MEDIUM, FONT_REGULAR, FONT_BOLD_DISPLAY } from '../../../../constants/StylesConstants';

const { extraLarge, regular, regularSmall, biggerlarge, mediumRegular, small } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    fontSize: extraLarge,
    marginTop: -v_scale(4)
  },
  container: {
    flex: 1,
    paddingTop: v_scale(16),
  },
  content: {
    paddingHorizontal: scale(16)
  },
  logoImage: {
    height: v_scale(130),
    width: scale(93)
  },
  imagesWrapper: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  topLabel: {
    fontFamily: FONT_MEDIUM,
    fontSize: regular,
    letterSpacing: -0.08,
    color: COLOR_BLACK,
    opacity: 0.75
  },
  smallLabel: {
    fontSize: regularSmall,
    opacity: 0.4
  },
  amount: {
    fontSize: biggerlarge,
    textAlign: 'right'
  },
  topBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingTop: v_scale(20),
    paddingBottom: v_scale(20)
  },
  cardImage: {
    width: scale(84),
    height: scale(84),
    borderRadius: scale(10),
    overflow: 'hidden'
  },
  itemCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: v_scale(16),
    paddingRight: scale(16),
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.2)'
  },
  contentCard: {
    width: '70%'
  },
  listContent: {
    paddingLeft: scale(16)
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: regular,
    letterSpacing: -0.41,
    opacity: 0.4
  },
  cardTitle: {
    fontFamily: FONT_BOLD_DISPLAY,
    fontSize: mediumRegular,
    letterSpacing: 0.35,
    color: COLOR_BLACK,
    marginVertical: v_scale(3)
  },
  contentTop: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  accentText: {
    fontFamily: FONT_MEDIUM,
    fontSize: small,
    letterSpacing: 0.07,
    color: '#BDBDBD',
    marginRight: scale(10)
  },
  blue: {
    color: COLOR_BLUE
  },
  noBorder: {
    borderBottomWidth: 0
  }
})