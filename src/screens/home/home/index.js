import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Card } from '../../../components';
import { DEMO_1_IMAGE, FULL_LOGO_ICON } from '../../../constants/Images';
import { navigate } from '../../../navigator/RootNavigation';
import { HomeFlow } from '../../../navigator/Keys';
import styles from './styles';

const DEMO = [{
  key: '0',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  date: 'СЕГОДНЯ',
  descriprion: 'Брелки, серебряные ложки и памятная посуда — это и еще много всего вы можете уви...'
}, {
  key: '1',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  date: 'СЕГОДНЯ',
  descriprion: 'Брелки, серебряные ложки и памятная посуда — это и еще много всего вы можете уви...'
}, {
  key: '2',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  date: 'СЕГОДНЯ',
  descriprion: 'Брелки, серебряные ложки и памятная посуда — это и еще много всего вы можете уви...'
}, {
  key: '3',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  date: 'СЕГОДНЯ',
  descriprion: 'Брелки, серебряные ложки и памятная посуда — это и еще много всего вы можете уви...'
}]

class HomeScreen extends Component {

  renderItemCard = ({ source, descriprion, title, accentText, date , key}, index) => {

    return (
      <TouchableOpacity 
        style={[styles.itemCard, (DEMO.length === index + 1) && styles.noBorder]} 
        key={key}
        activeOpacity={0.8}
        onPress={() => navigate(HomeFlow.HomeDetail)}
      >
        <View style={styles.contentCard}>
          <View style={styles.contentTop}>
            <Text style={[styles.accentText, styles.blue]}>{accentText}</Text>
            <Text style={styles.accentText}>{date}</Text>
          </View>
          <Text style={styles.cardTitle}>{title}</Text>
          <Text numberOfLines={3} style={styles.text}>{descriprion}</Text>
        </View>
        <Image
          source={source}
          resizeMode="cover"
          style={styles.cardImage}
        />
      </TouchableOpacity>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container>
        <View style={styles.container}>
          <ScrollView >
            <View style={styles.content}>
              <View style={styles.imagesWrapper}>
                <Image
                  source={FULL_LOGO_ICON}
                  resizeMode="contain"
                  style={styles.logoImage}
                />
              </View>
              <View style={styles.topBlock}>
                <View>
                  <Text style={styles.topLabel}>{"Доброе утро,".toUpperCase()}</Text>
                  <Text style={styles.title}>Александр</Text>
                </View>
                <View>
                  <Text style={[styles.topLabel, styles.smallLabel]}>{t('home_flow:your_points').toUpperCase()}</Text>
                  <Text style={[styles.title, styles.amount]}>3 550</Text>
                </View>
              </View>
              <Card
                source={DEMO_1_IMAGE}
                title={'Матрешки со скидкой 50%'}
                accentText={'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ'}
                descriprion={'Только с 5 по 9 Марта скидка на все матрешки в нашем магазине'}
                onPress={() => navigate(HomeFlow.HomeDetail)}
              />
            </View>
            <View style={styles.listContent}>
              {DEMO.map((item, index) => this.renderItemCard(item, index))}
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'home_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(HomeScreen),
);