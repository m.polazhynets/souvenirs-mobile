import { StyleSheet } from 'react-native';
import { COLOR_BLACK, COLOR_BLUE, COLOR_WHITE, COLOR_ORANGE } from '../../../../constants/Colors';
import variables, { scale, v_scale, FONT_REGULAR, FONT_MEDIUM } from '../../../../constants/StylesConstants';

const { large, extLarge, small } = variables.fontSize;

export default StyleSheet.create({
  content: {
    padding: scale(16),
  },
  text: {
    fontFamily: FONT_REGULAR,
    fontSize: large,
    letterSpacing: 0.38,
    color: COLOR_BLACK
  },
  image: {
    height: v_scale(240),
    width: '100%'
  },
  sliderWrapper: {
    position: 'relative'
  },
  dotsContainerStyle: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    paddingVertical: v_scale(18),
  },
  dotStyle: {
    width: scale(8),
    height: scale(8),
    borderRadius: scale(8),
    backgroundColor: COLOR_BLUE,
    marginHorizontal: -scale(5)
  },
  inactiveDotStyle: {
    backgroundColor: COLOR_WHITE,
    opacity: 0.2,
    width: scale(8),
    height: scale(8),
  },
  title: {
    ...variables.title,
    fontSize: extLarge,
  },
  typeText: {
    color: COLOR_ORANGE,
    fontFamily: FONT_MEDIUM,
    fontSize: small,
    letterSpacing: 0.07,
    marginTop: v_scale(10),
    marginBottom: v_scale(20)
  }
})