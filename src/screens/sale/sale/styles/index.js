import { StyleSheet, Platform } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper'
import { COLOR_BLACK, COLOR_WHITE  } from '../../../../constants/Colors';
import variables, { scale, v_scale, deviceHeight } from '../../../../constants/StylesConstants';

const { large, mainRegular } = variables.fontSize;

export default StyleSheet.create({
  title: {
    ...variables.title,
    paddingHorizontal: scale(16),
  },
  container: {
    flex: 1,
    paddingTop: v_scale(16),
  },
  content: {
    paddingHorizontal: scale(16)
  },

})