import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import { Container, Header, Card } from '../../../components';
import { navigate } from '../../../navigator/RootNavigation';
import { SaleFlow } from '../../../navigator/Keys';
import { DEMO_1_IMAGE } from '../../../constants/Images';
import styles from './styles';

const DEMO = [{
  key: '0',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  descriprion: 'Только с 5 по 9 Марта скидка на все матрешки в нашем магазине'
}, {
  key: '1',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  descriprion: 'Только с 5 по 9 Марта скидка на все матрешки в нашем магазине'
}, {
  key: '2',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  descriprion: 'Только с 5 по 9 Марта скидка на все матрешки в нашем магазине'
}, {
  key: '3',
  source: DEMO_1_IMAGE,
  title: 'Матрешки со скидкой 50%',
  accentText: 'АКЦИЯ',
  descriprion: 'Только с 5 по 9 Марта скидка на все матрешки в нашем магазине'
}]

class SaleScreen extends Component {
  render() {
    const { t } = this.props;

    return (
      <Container>
        <Header hideLogo hideBackArrow />
        <Text style={styles.title}>{t('sales_flow:sales')}</Text>
        <View style={styles.container}>
          <ScrollView style={styles.content} >
            {DEMO.map((item) => (
              <Card 
                {...item} 
                onPress={() => navigate(SaleFlow.SaleDetail)}
              />
            ))}
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'sales_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SaleScreen),
);