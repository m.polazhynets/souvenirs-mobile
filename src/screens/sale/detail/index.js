import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withTranslation } from 'react-i18next';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Container, Header } from '../../../components';
import { deviceWidth } from '../../../constants/StylesConstants';
import { DEMO_1_IMAGE } from '../../../constants/Images';
import styles from './styles';

const DEMO = [0, 1, 3]

class SaleDetailScreen extends Component {
  state = {
    activeSlide: 0
  }

  _renderItem = ({ item, index }) => {
    return (
      <View style={styles.slide}>
        <Image 
          style={styles.image} 
          source={DEMO_1_IMAGE}
          resizeMode={'cover'}
        />
      </View>
    );
  }

  pagination = () => {
    const { activeSlide } = this.state;

    return (
      <Pagination
        dotsLength={DEMO.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.dotsContainerStyle}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        // inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    );
  }

  renderSlider = () => {
    return (
      <View style={styles.sliderWrapper}>
        <Carousel
          data={DEMO}
          renderItem={this._renderItem}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          onSnapToItem={(index) => this.setState({ activeSlide: index })}
        />
        {this.pagination()}
      </View>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <Container hideBackground>
        <Header hideLogo/>
        <ScrollView style={styles.container}>
          {this.renderSlider()}
          <View style={styles.content}>
            <Text style={styles.title}>Матрешки со скидкой 50%</Text>
            <Text style={styles.typeText}>АКЦИЯ</Text>
            <Text style={styles.text}>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue.</Text>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {

    },
    dispatch,
  );

export default withTranslation(['common', 'sale_flow'])(
  connect(mapStateToProps, mapDispatchToProps)(SaleDetailScreen),
);