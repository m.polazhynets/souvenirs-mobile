// @flow
/**
 * Client
 * @module Client
 */
import axios from 'axios';
import api_config from '../config';

/**
 * Axios data
 *
 * @param {string} url
 * @param {Object} options
 * @param {string} [options.method] - Request method ( GET, POST, PUT, ... ).
 * @param {string} [options.payload] - Request body.
 * @param {Object} [options.headers]
 *
 * @returns {Promise}
 */
export function request(url, options = {}) {
  const config = {
    method: 'GET',
    ...options,
  };
  const errors = [];

  if (!url) {
    errors.push('url');
  }

  if (!config.payload && (config.method !== 'GET' && config.method !== 'DELETE')) {
    errors.push('payload');
  }

  if (errors.length) {
    throw new Error(`Error! You must pass \`${errors.join('`, `')}\``);
  }

  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    ...config.headers,
  };

  const params = {
    headers,
    method: config.method,
    withCredentials: true,
  };

  return axios({
    url: /^(https?:\/\/)/.test(url) ? url : `${api_config.domain}${url}`,
    ...params
  })
  .then(async response => {
    if (response.data.status >= 400) throw response.data;
    return response.data;
  })
  .catch(async err => {
    throw err;
  })
}