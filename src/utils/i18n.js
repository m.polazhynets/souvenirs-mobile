import { I18nManager } from 'react-native';
import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import * as RNLocalize from "react-native-localize";
import ru from './translations/ru.json';

const CurrentLocale = RNLocalize.getLocales()

const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: cb => cb(CurrentLocale ? CurrentLocale[0].languageCode : 'ru'),
  init: () => {},
  cacheUserLanguage: () => {},
};

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'ru',
    debug: true,
    resources: { ru },
  });

I18nManager.forceRTL(false);
export default i18n;
