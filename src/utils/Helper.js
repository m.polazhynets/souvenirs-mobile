import _ from 'underscore';

export const validateText = (val = "", minLength = 1) => {
  let re = /^[a-zA-Zа-яА-ЯЁё - "'\n]+(([a-zA-Z ])?[' -.][a-zA-Z]*)*$/;

  return (re.test(val) && val.length >= minLength);
}

export const validateEmail = (val) => {
  let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return (val.length) ? re.test(val) : false;
}

export const validatePassword = (pass = "", confirmVal = '', minLength = 8) => {
  let res = (pass.indexOf(" ") === -1 && pass.length >= minLength)

  return (confirmVal.length) ? (res && confirmVal === pass) : res;
}
