import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { 
  HomeScreen,
  HomeDetailScreen,
} from '../../Screens';
import { HomeFlow } from '../../Keys';

const Stack = createStackNavigator();

export default HomeStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <Stack.Screen name={HomeFlow.Home} component={HomeScreen} />
      <Stack.Screen name={HomeFlow.HomeDetail} component={HomeDetailScreen} />
    </Stack.Navigator>
  );
}