import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { 
  MenuScreen,
  LoyaltyRulesScreen,
  PrivacyPolicyScreen,
  TermsOfUseScreen,
  ReviewScreen,
  FAQsScreen,
  AboutScreen
} from '../../Screens';
import { MenuFlow } from '../../Keys';

const Stack = createStackNavigator();

export default MenuStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <Stack.Screen name={MenuFlow.Menu} component={MenuScreen} />
      <Stack.Screen name={MenuFlow.About} component={AboutScreen} />
      <Stack.Screen name={MenuFlow.LoyaltyRules} component={LoyaltyRulesScreen} />
      <Stack.Screen name={MenuFlow.PrivacyPolicy} component={PrivacyPolicyScreen} />
      <Stack.Screen name={MenuFlow.TermsOfUse} component={TermsOfUseScreen} />
      <Stack.Screen name={MenuFlow.Review} component={ReviewScreen} />
      <Stack.Screen name={MenuFlow.FAQs} component={FAQsScreen} />
    </Stack.Navigator>
  );
}