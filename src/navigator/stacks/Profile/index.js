import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { 
  ProfileScreen,
  SettingsScreen,
  NotifySettingsScreen,
  ArrivedHistoryScreen,
  TransactionScreen,
  OrderToOfficeScreen,
  InviteFriendsProfileScreen,
  StatisticsScreen
} from '../../Screens';
import { ProfileFlow } from '../../Keys';

const Stack = createStackNavigator();

export default ProfileStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <Stack.Screen name={ProfileFlow.Profile} component={ProfileScreen} />
      <Stack.Screen name={ProfileFlow.Settings} component={SettingsScreen} />
      <Stack.Screen name={ProfileFlow.NotifySettings} component={NotifySettingsScreen} />
      <Stack.Screen name={ProfileFlow.ArrivedHistory} component={ArrivedHistoryScreen} />
      <Stack.Screen name={ProfileFlow.Transaction} component={TransactionScreen} />
      <Stack.Screen name={ProfileFlow.OrderToOffice} component={OrderToOfficeScreen} />
      <Stack.Screen name={ProfileFlow.InviteFriendsProfile} component={InviteFriendsProfileScreen} />
      <Stack.Screen name={ProfileFlow.Statistics} component={StatisticsScreen} />
    </Stack.Navigator>
  );
}