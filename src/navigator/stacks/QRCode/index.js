import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { 
  QrCodeScreen,
  HowItWorksScreen
} from '../../Screens';
import { QRCodeFlow } from '../../Keys';

const Stack = createStackNavigator();

export default QRCodeStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <Stack.Screen name={QRCodeFlow.QRCode} component={QrCodeScreen} />
      <Stack.Screen name={QRCodeFlow.HowItWorks} component={HowItWorksScreen} />
    </Stack.Navigator>
  );
}