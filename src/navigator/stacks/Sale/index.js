import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { 
  SaleScreen,
  SaleDetailScreen
} from '../../Screens';
import { SaleFlow } from '../../Keys';

const Stack = createStackNavigator();

export default HomeStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <Stack.Screen name={SaleFlow.Sale} component={SaleScreen} />
      <Stack.Screen name={SaleFlow.SaleDetail} component={SaleDetailScreen} />
    </Stack.Navigator>
  );
}