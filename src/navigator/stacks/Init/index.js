import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { CardStyleInterpolators } from '@react-navigation/stack';
import { 
  LogInScreen, 
  SignUpScreen,
  SignUpPrivateDataScreen,
  NewPasswordScreen, 
  ConfirmCodeScreen,
  SecurityScreen,
  EnableBiometricScreen,
  InviteFriendsScreen,
  ActiveBonusProgramScreen,
} from '../../Screens';
import { InitFlow } from '../../Keys';

const Stack = createStackNavigator();

export default InitStack = () => {

  return (
    <Stack.Navigator 
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true,
        
      }}
    >
      <Stack.Screen name={InitFlow.LogIn} component={LogInScreen} />
      <Stack.Screen name={InitFlow.SignUp} component={SignUpScreen} />
      <Stack.Screen name={InitFlow.SignUpPrivateData} component={SignUpPrivateDataScreen} />
      <Stack.Screen name={InitFlow.NewPassword} component={NewPasswordScreen} />
      <Stack.Screen name={InitFlow.ConfirmCode} component={ConfirmCodeScreen} />
      <Stack.Screen name={InitFlow.Security} component={SecurityScreen} />
      <Stack.Screen name={InitFlow.EnableBiometric} component={EnableBiometricScreen} />
      <Stack.Screen name={InitFlow.InviteFriends} component={InviteFriendsScreen} />
      <Stack.Screen name={InitFlow.ActiveBonusProgram} component={ActiveBonusProgramScreen} />
    </Stack.Navigator>
  );
}