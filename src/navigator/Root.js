import React from 'react';
import { Image, Text, StyleSheet, Platform } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { InitStack, HomeStack, SaleStack, QRCodeStack, ProfileStack, MenuStack } from './stacks';
import { StartScreen } from './Screens';
import { FlowKeys, HomeFlow, SaleFlow, QRCodeFlow, ProfileFlow, MenuFlow } from './Keys';
import variables, { scale, FONT_MEDIUM, v_scale } from '../constants/StylesConstants';
import { COLOR_GRAY_2, COLOR_BLUE, COLOR_ORANGE } from '../constants/Colors';
import i18n from '../utils/i18n';
import {
  HOME_TAB_ICON,
  HOME_TAB_ACTIVE_ICON,
  SALE_TAB_ICON,
  SALE_TAB_ACTIVE_ICON,
  CODE_TAB_ICON,
  CODE_TAB_ACTIVE_ICON,
  PROFILE_TAB_ICON,
  PROFILE_TAB_ACTIVE_ICON,
  MENU_TAB_ICON,
  MENU_TAB_ACTIVE_ICON
} from '../constants/Images';
const { smaller } = variables.fontSize;

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainStack = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: styles.tabsStyles,
      }}
    >
      <Tab.Screen
        name={HomeFlow.Home}
        component={HomeStack}
        options={({route}) => ({ 
          tabBarVisible: (!route.state) || route.state.index === 0,
          tabBarIcon: ({ focused }) => (
            <Image
              source={focused ? HOME_TAB_ACTIVE_ICON : HOME_TAB_ICON}
              style={styles.tabIcon}
              resizeMode="contain"
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Text style={[styles.label, focused && { color: COLOR_BLUE }]}>{i18n.t('common:tabs.home')}</Text>
          )
        })}
      />
      <Tab.Screen
        name={SaleFlow.Sale}
        component={SaleStack}
        options={({route}) => ({
          tabBarVisible: (!route.state) || route.state.index === 0,
          tabBarIcon: ({ focused }) => (
            <Image
              source={focused ? SALE_TAB_ACTIVE_ICON : SALE_TAB_ICON}
              style={styles.tabIcon}
              resizeMode="contain"
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Text style={[styles.label, { color: COLOR_ORANGE }, focused && { color: COLOR_BLUE }]}>{i18n.t('common:tabs.sales')}</Text>
          )
        })}
      />
      <Tab.Screen
        name={QRCodeFlow.QRCode}
        component={QRCodeStack}
        options={({route}) => ({
          tabBarVisible: (!route.state) || route.state.index === 0,
          tabBarIcon: ({ focused }) => (
            <Image
              source={focused ? CODE_TAB_ACTIVE_ICON : CODE_TAB_ICON}
              style={styles.tabIcon}
              resizeMode="contain"
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Text style={[styles.label, focused && { color: COLOR_BLUE }]}>{i18n.t('common:tabs.qr_code')}</Text>
          )
        })}
      />
      <Tab.Screen
        name={ProfileFlow.Profile}
        component={ProfileStack}
        options={({route}) => ({
          tabBarVisible: (!route.state) || route.state.index === 0,
          tabBarIcon: ({ focused }) => (
            <Image
              source={focused ? PROFILE_TAB_ACTIVE_ICON : PROFILE_TAB_ICON}
              style={styles.tabIcon}
              resizeMode="contain"
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Text style={[styles.label, focused && { color: COLOR_BLUE }]}>{i18n.t('common:tabs.profile')}</Text>
          )
        })}
      />
      <Tab.Screen
        name={MenuFlow.Menu}
        component={MenuStack}
        options={({ route }) => ({
          tabBarVisible: (!route.state) || route.state.index === 0,
          tabBarIcon: ({ focused }) => (
            <Image
              source={focused ? MENU_TAB_ACTIVE_ICON : MENU_TAB_ICON}
              style={styles.tabIcon}
              resizeMode="contain"
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Text style={[styles.label, focused && { color: COLOR_BLUE }]}>{i18n.t('common:tabs.more')}</Text>
          )
        })}
      />
    </Tab.Navigator>
  )
}

function RootStack({ currentRouteName }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardOverlayEnabled: true
      }}
    >
      <Stack.Screen name={FlowKeys.Start} component={StartScreen} />
      <Stack.Screen name={FlowKeys.Main} component={MainStack} currentRouteName={currentRouteName} />
      <Stack.Screen name={FlowKeys.Initial} component={InitStack} currentRouteName={currentRouteName} />
    </Stack.Navigator>
  );
}

export default RootStack

const styles = StyleSheet.create({
  label: {
    color: COLOR_GRAY_2,
    fontFamily: FONT_MEDIUM,
    fontSize: smaller,
    letterSpacing: 0.16
  },
  tabIcon: {
    width: scale(28),
    height: scale(28)
  },
  tabsStyles: {
    borderColor: 'rgba(0, 0, 0, 0.2)',
    paddingBottom: (Platform.OS === 'android') ? v_scale(7) : (isIphoneX()) ? v_scale(34) : v_scale(5),
  }
})