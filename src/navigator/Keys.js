export const FlowKeys = {
  Start: 'StartFlow',
  Initial: 'InitialFlow',
  Main: 'MainFlow',
}

export const InitFlow = {
  LogIn: 'LogIn',
  SignUp: 'SignUp',
  SignUpPrivateData: 'SignUpPrivateData',
  NewPassword: 'NewPassword',
  ConfirmCode: 'ConfirmCode',
  Security: 'Security',
  EnableBiometric: 'EnableBiometric',
  InviteFriends: 'InviteFriends',
  ActiveBonusProgram: 'ActiveBonusProgram',
}

export const HomeFlow = {
  Home: 'Home',
  HomeDetail: 'HomeDetail'
}

export const SaleFlow = {
  Sale: 'Sale',
  SaleDetail: 'SaleDetail'
}

export const QRCodeFlow = {
  QRCode: 'QRCode',
  HowItWorks: 'HowItWorks'
}

export const ProfileFlow = {
  Profile: 'Profile',
  Settings: 'Settings',
  NotifySettings: 'NotifySettings',
  ArrivedHistory: 'ArrivedHistory',
  Transaction: 'Transaction',
  OrderToOffice: 'OrderToOffice',
  InviteFriendsProfile: 'InviteFriendsProfile',
  Statistics: 'Statistics'
}

export const MenuFlow = {
  Menu: 'Menu',
  About: 'About',
  LoyaltyRules: 'LoyaltyRules',
  PrivacyPolicy: 'PrivacyPolicy',
  TermsOfUse: 'TermsOfUse',
  Review: 'Review',
  FAQs: 'FAQs'
}


